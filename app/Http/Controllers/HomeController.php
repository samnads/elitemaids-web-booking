<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home(Request $request)
    {
        //$data = customerApiCall('data', [])['result'];
        $data['api_data'] = customerApiCall('data', [])['result'];
        $data['api_area_list'] = customerApiCall('area_list', [])['result'];
        $data['banners'] = $data['api_data']['subscription_packages_and_special_offers'];
        //dd($data);
        return view('home', $data);
    }
}
