<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Config;
use Session;

class InvoiceController extends Controller
{
    public function invoice_payment_entry(Request $request)
    {
        $data = $request->all();
        $data['api_data'] = customerApiCall('data', [])['result'];
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('POST', Config::get('url.api_url') . 'customer/get-customer-odoo', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'customerId' => $data['id'],
            ],

        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        $data['customer_odoo'] = $responseBody['data'];
        return view('invoice.invoice-payment-entry', $data);
    }
    public function invoice_payment_save(Request $request)
    {
        $data = $request->all();
        $data['api_data'] = customerApiCall('data', [])['result'];
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('POST', Config::get('url.api_url') . 'customer/save-invoice-pay', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'inv_id' => $request->input('inv_id'),
                'customerId' => $request->input('customerId'),
                'amount' => $request->input('amount'),
                'description' => $request->input('description')
            ]
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        $data['save_invoice_pay'] = $responseBody['data'];
        return view('invoice.invoice-payment-checkout', $data);
    }
    public function invoice_success($payment_id){
        $data['api_data'] = customerApiCall('data', [])['result'];
        $params['params']['payment_id'] = $payment_id;
        $online_payment_by_id = customerApiCall('online-payment-by-id', $params)['result'];
        $data['online_payment'] = @$online_payment_by_id['online_payment'];
        $data['customer'] = @$online_payment_by_id['customer'];
        $data['customer_address'] = @$online_payment_by_id['customer_address'];
        //dd($data['online-payment']);
        if(strtolower($data['online_payment']['payment_status']) != 'success'){
            return view('invoice.invoice-payment-failed', $data);
        }
        return view('invoice.invoice-payment-success', $data);
    }
    public function invoice_failed($payment_id)
    {
        $data['api_data'] = customerApiCall('data', [])['result'];
        $params['params']['payment_id'] = $payment_id;
        $online_payment_by_id = customerApiCall('online-payment-by-id', $params)['result'];
        $data['online_payment'] = @$online_payment_by_id['online_payment'];
        $data['customer'] = @$online_payment_by_id['customer'];
        $data['customer_address'] = @$online_payment_by_id['customer_address'];
        //dd($data['online-payment']);
        return view('invoice.invoice-payment-failed', $data);
    }
}
