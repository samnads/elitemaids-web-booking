<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ProfileController extends Controller {
    public function profile(Request $request) {
        $data['api_data'] = customerApiCall('data', [])['result'];
        $data['api_area_list'] = customerApiCall('area_list', [])['result'];
        return view('account.profile', $data);
    }

    public function personal_details(Request $request) {
        $data['api_data'] = customerApiCall('data', [])['result'];
        $data['api_area_list'] = customerApiCall('area_list', [])['result'];
        return view('account.personal', $data);
    }

    public function manage_address(Request $request) {
        $data['api_data'] = customerApiCall('data', [])['result'];
        $data['api_area_list'] = customerApiCall('area_list', [])['result'];
        return view('account.manage-address', $data);
    }

    public function bookings(Request $request, $filter) {
        //dd($filter);
        if($filter == 'upcoming') {
            $params['params']['booking_type'] = 'waiting';
            $data['booking_history'] = customerApiCall('booking_history', $params, $request->method());
            $data['api_data'] = customerApiCall('data', [])['result'];
            //dd($data);
            return view('bookings.upcoming', $data);
        } else if($filter == 'cancelled') {
            $params['params']['booking_type'] = 'cancelled';
            $data['booking_history'] = customerApiCall('booking_history', $params, $request->method());
            $data['api_data'] = customerApiCall('data', [])['result'];
            //dd($data);
            return view('bookings.cancelled', $data);
        }
        else if($filter == 'past') {
            $params['params']['booking_type'] = 'done';
            $data['booking_history'] = customerApiCall('booking_history', $params, $request->method());
            $data['api_data'] = customerApiCall('data', [])['result'];
            //dd($data);
            return view('bookings.past', $data);
        }
    }
    public function booking_success(Request $request, $reference_id) {
        if(!session('customer_id')){
            // may be from app web view
            echo 'Success !';
            echo '<br><b style="color:green">'.url()->full().'</b>';
            exit;
        }
        $params['params']['reference_id'] = $reference_id;
        $data = customerApiCall('booking_details_by_ref', $params, 'POST')['result']['data'];
        $data['api_data'] = customerApiCall('data', [])['result'];
        $data['booking'] = $data['bookings'][0];
        $data['reference_id'] = $reference_id;
        $data['booking_status'] = 'Confirmed';
        $data['payment_status'] = 'Payment Pending';
        if ($data['booking']['payment_type_id'] == 1) {
            $data['booking_status'] = 'Received';
        }
        else if ($data['booking']['payment_type_id'] != 1) {
            $data['payment_status'] = 'Paid';
        }
        //dd($data);
        return view('account.booking.success', $data);
    }
    public function booking_failed(Request $request, $reference_id) {
        if (!session('customer_id')) {
            // may be from app web view
            echo 'Failed !';
            echo '<br><b style="color:red">' . url()->full() . '</b>';
            exit;
        }
        $params['params']['reference_id'] = $reference_id;
        $data = customerApiCall('booking_details_by_ref', $params, 'POST')['result']['data'];
        $data['api_data'] = customerApiCall('data', [])['result'];
        $data['booking'] = $data['bookings'][0];
        $data['reference_id'] = $reference_id;
        $data['booking_status'] = 'Payment Failed';
        $data['payment_status'] = 'Payment Pending';
        //dd($data);
        return view('account.booking.failed', $data);
    }

    public function profile_edit_popup(Request $request){

    if ($request->isMethod('POST')) {
        $allValues = $request->all();
        session()->put('selected_image', $request->get('selected_image'));
        Log::info('All Values: ' . print_r($allValues, true));
        return response()->json([
        'success' => true,
        ]);
    }

    return view('popups.profile_edit_popup');
    }

}
