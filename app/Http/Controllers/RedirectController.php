<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Response;

class RedirectController extends Controller
{
    public function redirect_based_on_action(Request $request, $action)
    {
        $redirect_url = urldecode($request->url);
        $parts = parse_url($redirect_url);
        parse_str($parts['query'], $query);
        if($action == 'telr-gateway-entry'){
            Session::put('telr_order_ref', $query['o']);
            Session::put('telr_order_url', $redirect_url);
            // these session vars are used by the process page
            return redirect($redirect_url);
        }
    }
}
