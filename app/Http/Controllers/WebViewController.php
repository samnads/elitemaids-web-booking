<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WebViewController extends Controller
{
    public function google_pay_webview(Request $request)
    {
        $data['api_data'] = customerApiCall('data', [])['result'];
        return view('webviews.google_pay_webview', $data);
    }
}
