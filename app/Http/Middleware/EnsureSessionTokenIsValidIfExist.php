<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class EnsureSessionTokenIsValidIfExist
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (session('customer_id')) {
            $params['params']['id'] = session('customer_id');
            $params['params']['token'] = session('customer_token');
            $response = customerApiCall('validate_token', $params);
            if (@$response['result']['status'] == "success") {
                return $next($request);
            } else {
                $request->session()->flush();
                return redirect()->route('home', ['error' => 'expired_token']);
            }
        } else {
            return $next($request);
        }
    }
}
