<?php
use Cache as Cache;

function customerApiCall($endpoint, $params, $method = 'GET')
{
    $cached_apis = ['data', 'area_list'];
    if ($endpoint == 'validate_token') {
        //dd($params);
    }
    /******************************************** */
    // check and return cached data if found
    if (in_array($endpoint, $cached_apis)) {
        if (Cache::get($endpoint)) {
            return Cache::get($endpoint);
        }
    }
    /******************************************** */
    // for ajax (from web) and direct curl call purposes
    $client = new \GuzzleHttp\Client([
        'verify' => false,
    ]);
    /******************************************** */
    // append with existing
    $params['params']['id'] = @$params['params']['id'] ?: @session('customer_id');
    $params['params']['token'] = @$params['params']['token'] ?: @session('customer_token');
    $params['params']['platform'] = 'web';
    /******************************************** */
    $response = $client->request($method, Config::get('url.api_url') . 'customer/' . $endpoint, [
        'headers' => [
            'cache-control' => 'no-cache',
            'Content-Type' => 'application/x-www-form-urlencoded',
        ],
        'body' => json_encode($params),
    ]);
    $responseBody = json_decode((string) $response->getBody(), true);
    /******************************************** */
    // check and return cached data if found
    if (in_array($endpoint, $cached_apis)) {
        Cache::put($endpoint, $responseBody, 86400);
    }
    /******************************************** */
    return $responseBody;
}
function apiCall($full_endpoint, $params, $method = 'GET')
{
    /******************************************** */
    // for ajax (from web) and direct curl call purposes
    $client = new \GuzzleHttp\Client([
        'verify' => false,
    ]);
    /******************************************** */
    // append with existing
    $params['params']['id'] = @$params['params']['id'] ?: @session('customer_id');
    $params['params']['token'] = @$params['params']['token'] ?: @session('customer_token');
    /******************************************** */
    $response = $client->request($method, Config::get('url.api_url') . $full_endpoint, [
        'headers' => [
            'cache-control' => 'no-cache',
            'Content-Type' => 'application/x-www-form-urlencoded',
        ],
        'body' => json_encode($params),
    ]);
    $responseBody = json_decode((string) $response->getBody(), true);
    /******************************************** */
    return $responseBody;
}
function googlePayEnvironment()
{
    $url_string = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
    if (strpos($url_string, 'demo/') !== false || strpos($url_string, '127.0.0.1') !== false) {
        return 'TEST';
    } else {
        return 'PRODUCTION';
    }
}
function isDemo()
{
    $locals = array(
        '127.0.0.1:8095',
        '127.0.0.1',
        'localhost',
        '::1',
        '-demo'
    );
    if (in_array(@$_SERVER['HTTP_HOST'], $locals)) {
        return true;
    }
    return false;
}
function debugPaymentModes()
{
    // for not showing payment modes
    // return array of payment ids
    // 3 - Apple Pay
    return [];
}
function debugPaymentModeForCustomers()
{
    // for showing debug payment modes only for users
    // return array of payment ids
    // 20506 - Ayah Manea ; Live Admin Customer
    // 39504 - Samnad S ; Live Developer
    return ['20506', '39504'];
}

function hidePaymentModes($usage = null)
{
    /**
     * Hide specific payment modes for specific use cases
     * helpful for debugging
     * 1 - Cash
     * 2 - Card
     * 3 - Apple Pay
     * 4 - Google Pay
     * 5 - Tamara
     * 6 - Card (Telr)
     * 7 - Card + Apple Pay (Telr)
     */
    if ($usage == 'normal-model') {
        // normal services
        return [];
    } else if ($usage == 'package-model') {
        // package services
        return [];
    } else if ($usage == 'subs-package-model') {
        // subscription package services
        return [1];
    } else if ($usage == 'invoice-payment') {
        // Invoice payment link
        return [1, 2, 3, 4, 5];
    } else {
        return [];
    }
}
function numberFormat($number, $decimal = 2)
{
    // custom number format
    return number_format($number, $decimal, '.', ',');
}

function whatsappLink($number)
{
    return 'https://wa.me/'.preg_replace("/[^0-9]/", "", $number);
}

function telephoneLink($number)
{
    return 'tel:'.preg_replace('/[^0-9]/','',$number);
}