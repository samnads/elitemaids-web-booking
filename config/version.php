<?php
return [
    'js' => isDemo() === true ? time() : '1.53',
    'css' => isDemo() === true ? time() : '1.5',
    'img' => isDemo() === true ? time() : '1.5',
]
    ?>