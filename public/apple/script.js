var applePayButton = document.getElementById('ckocom_applePay');
var applePayNotActivated = document.getElementById('ckocom_applePay_not_actived');
var applePayNotPossible = document.getElementById('ckocom_applePay_not_possible');

var MERCHANT_ID = 'merchant.com.emaid.elitmaidsUser';

var PRODUCT_PRICE = 12.0;
var SHOP_NAME = 'Johnny Shop';
var SHIPPING_METHODS = [];
var CURRENT_SHIPPING_METHOD = {};

/**
 * Check ApplePay availability
 */
displayApplePayButton();

/**
 * When the ApplePay button is clicked, begin a ApplePaySession 
 */
applePayButton.addEventListener('click', function () {
	getApplePayConfig(function (config) {
		var applePaySession = new ApplePaySession(3, config);
		handleApplePayEvents(applePaySession);
		applePaySession.begin();
	});
});

/**
 * This function contains the handlers for all ApplePay events
 *
 * @param {object} session - ApplePaySession 
 */
function handleApplePayEvents(session) {
	/**
	 * An event handler that is called when the payment sheet is displayed.
	 * 
	 * @param {object} event - The event contains the validationURL
	 */
	 console.log(session);
	session.onvalidatemerchant = function (event) {
		performAppleUrlValidation(event.validationURL, function (merchantSession) {
			session.completeMerchantValidation(merchantSession);
		});
	};

	/**
	 * An event handler that is called when a shipping contact is selected in the payment sheet.
	 * 
	 * @param {object} event - The event contains minimum details about the address 
	 * so that the shipping option can be loaded for a specific region
	 */
	session.onshippingcontactselected = function (event) {
		getShippingMethods(event.shippingContact.countryCode, function (shipping) {
			// update SHIPPING_METHODS
			SHIPPING_METHODS = shipping.methods;
			CURRENT_SHIPPING_METHOD = shipping.methods[0];
			var status = ApplePaySession.STATUS_SUCCESS;
			var newTotal = {
				type: 'final',
				label: SHOP_NAME,
				amount: calculateTotal(PRODUCT_PRICE, shipping.methods[0].amount)
			};
			var newLineItems = [
				{
					type: 'final',
					label: 'Subtotal',
					amount: PRODUCT_PRICE
				},
				{
					type: 'final',
					label: shipping.methods[0].label,
					amount: shipping.methods[0].amount
				}
			];
			session.completeShippingContactSelection(status, shipping.methods, newTotal, newLineItems);
		});
	};

	/**
	 * An event handler that is called when a shipping method is selected.
	 * 
	 * @param {object} event - The event contains the shippingMethod selected
	 */
	session.onshippingmethodselected = function (event) {
		var status = ApplePaySession.STATUS_SUCCESS;

		SHIPPING_METHODS.forEach(function (method) {
			// Iterate the present shipping methods to find details about the selected one
			if (method.label === event.shippingMethod.label) {
				// update current shipping method
				CURRENT_SHIPPING_METHOD = method;
				var newTotal = {
					type: 'final',
					label: SHOP_NAME,
					amount: calculateTotal(PRODUCT_PRICE, CURRENT_SHIPPING_METHOD.amount)
				};
				var newLineItems = [
					{
						type: 'final',
						label: 'Subtotal',
						amount: PRODUCT_PRICE
					},
					{
						type: 'final',
						label: CURRENT_SHIPPING_METHOD.label,
						amount: CURRENT_SHIPPING_METHOD.amount
					}
				];
				session.completeShippingMethodSelection(status, newTotal, newLineItems);
			}
		});
	};

	/**
	 * An event handler that is called when a new payment method is selected.
	 * 
	 * @param {object} event - The event contains the payment method selected
	 */
	session.onpaymentmethodselected = function (event) {
		// base on the card selected the total can be change, if for example you
		// plan to charge a fee for credit cards for example
		var newTotal = {
			type: 'final',
			label: SHOP_NAME,
			amount: calculateTotal(PRODUCT_PRICE, CURRENT_SHIPPING_METHOD.amount)
		};
		var newLineItems = [
			{
				type: 'final',
				label: 'Subtotal',
				amount: PRODUCT_PRICE
			},
			{
				type: 'final',
				label: CURRENT_SHIPPING_METHOD.label,
				amount: CURRENT_SHIPPING_METHOD.amount
			}
		];

		session.completePaymentMethodSelection(newTotal, newLineItems);
	};

	/**
	 * An event handler that is called when the user has authorized the Apple Pay payment
	 *  with Touch ID, Face ID, or passcode.
	 */
	session.onpaymentauthorized = function (event) {
		// demo an ApplePay error
		// simulateApplePayError(session)
		console.log(event);

		talkToCheckoutApi(event.payment.token.paymentData, function (outcome) {
			if (outcome.status === 'Approved') {
				status = ApplePaySession.STATUS_SUCCESS;
			}
		});

		session.completePayment(status);
	};

	/**
	 * An event handler that is automatically called when the payment UI is dismissed.
	 */
	session.oncancel = function (event) {
		// popup dismissed
	};
}

/**
 * This checks if ApplePay is available in the browser
 *
 * @return {boolean} A good string
 * 
 */
function isApplePayAvailable() {
	return window.ApplePaySession && ApplePaySession.canMakePayments();
}

/**
 * This will make the ApplePay button visible
 */
function displayApplePayButton() {
	applePayButton.style.display = 'block';
}

/**
 * This will hide the ApplePay button
 */
function hideApplePayButton() {
	applePayButton.style.display = 'none';
}

/**
 * This will display the ApplePay not activated message
 */
function displayApplePayNotActivated() {
	applePayNotActivated.style.display = 'block';
}

/**
 * This will display the ApplePay not possible message
 */
function displayApplePayNotPossible() {
	applePayNotPossible.style.display = 'block';
}

/**
 * This gets the ApplePay configuration 
 * 
 *  @param {function} callback A callback function used to return the 
 * configuration in cas a async call is required
 *
 * @return {object} The payment request configuration
 * 
 */
function getApplePayConfig(callback) {
	// Create a promise, so  you can easily do AJAX calls to get the configuration
	// simulate HTTP call with a timeout
	setTimeout(function () {
		callback({
			currencyCode: 'GBP',
			countryCode: 'GB',
			merchantCapabilities: ['supports3DS', 'supportsEMV', 'supportsCredit', 'supportsDebit'],
			supportedNetworks: ['amex', 'masterCard', 'visa'],
			shippingType: 'shipping',
			requiredBillingContactFields: ['postalAddress', 'name', 'phoneticName'],
			requiredShippingContactFields: ['postalAddress', 'name', 'phone', 'email'],
			total: {
				label: SHOP_NAME,
				amount: PRODUCT_PRICE,
				type: 'final'
			}
		});
	}, 1000);
}

/**
 * This gets the available shipping methods based on a country code.
 * Other criteria can be used to decide the shipping methods.
 * 
 *  @param {string} countryCode A string representing the country code 
 *  @param {function} callback A callback function used to return the 
 * shipping methods in cas a async call is required
 *
 * @return {object} The payment request configuration
 * 
 */
function getShippingMethods(countryCode, callback) {
	// simulate async call to get the shipping methods
	setTimeout(function () {
		if (countryCode === 'GB') {
			callback({
				methods: [
					{
						label: 'Express Shipping',
						amount: '5.00',
						detail: 'Arrives in 2-3 days',
						identifier: 'expressShipping'
					},
					{
						label: 'Free Standard Shipping',
						amount: '0.00',
						detail: 'Arrives in 5-7 days',
						identifier: 'standardShipping'
					}
				]
			});
		} else {
			callback({
				methods: [
					{
						label: 'Free Standard Shipping',
						amount: '0.00',
						detail: 'Arrives in 5-7 days',
						identifier: 'standardShipping'
					}
				]
			});
		}
	}, 1000);
}

/**
 * This adds the value of the subtotal adn shipping method in order
 * to get the total of the transaction.
 * 
 *  @param {number} subtotal The subtotal of the transaction
 *  @param {string} shipping The shipping cost of the transaction
 *
 * @return {number} The total value of the transaction
 * 
 */
function calculateTotal(subtotal, shipping) {
	return (parseFloat(subtotal) + parseFloat(shipping)).toFixed(2);
}

/**
 * This performs the URL validation with the dynamic url generated
 * by ApplePay
 * 
 *  @param {number} valURL The subtotal of the transaction
 *  @param {string} callback A callback function used to return the 
 * outcome of the validation
 *
 */
function performAppleUrlValidation(valURL, callback) {
	var xhr = new XMLHttpRequest();
	xhr.onload = function () {
		var data = JSON.parse(this.responseText);
		callback(data);
	};
	xhr.open('GET', 'appleSession.php?u=' + valURL);
	xhr.send();
}

/**
 * This triggers an error in the ApplePay popup 
 * 
 *  @param {object} session The ApplePay session
 *
 */
function simulateApplePayError(session) {
	var err = new ApplePayError('shippingContactInvalid', 'postalAddress', 'PostalCode not matching.');
	session.completePayment({
		status: ApplePaySession.STATUS_FAILURE,
		errors: [err]
	});
}

/**
 * This is where you would normally perform the API calls to Checkout.com
 * and you would indicate the outcome
 * 
 *  @param {function} callback The ApplePay session
 *
 */
function talkToCheckoutApi(token, callback) {
	// simulate async call to charge the customer

	$.ajax({
		url: "applecharge.php",
		type: "POST",
		data: {
			version: token.version,
			data: token.data,
			signature: token.signature,
			ephemeralPublicKey: token.header.ephemeralPublicKey,
			publicKeyHash: token.header.publicKeyHash,
			transactionId: token.header.transactionId,
		},
		success: function (data) {
			console.log(data);
			callback({
				status: 'Approved'
			});
		},
		error: function (err) {
			console.log(err);
			callback({
				status: 'Approved'
			});
		},
	});
}
