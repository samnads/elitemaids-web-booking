var owl = $(".service-categories");
owl.owlCarousel({
    nav: false,
    loop: false,
    dots: true,
    margin: 15,
    autoplay: true,
    autoplayTimeout: 4000,
    smartSpeed: 800,
    autoplayHoverPause: true,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 2
        },
        1000: {
            items: 2
        },
        1100: {
            items: 2
        },
        1200: {
            items: 5,
            margin: 30
        }
    }
});
var owl = $("#banner");
owl.owlCarousel({
    items: 1,
    loop: true,
    margin: 0,
    autoplay: true,
    autoplayTimeout: 4000,
    autoplayHoverPause: true
});