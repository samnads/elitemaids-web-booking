// step button actions
$('[data-action="next-step"]').click(function () {
    booking_form = $('#booking-form');
    let current_step = Number($(this).attr('data-step'));
    if (isStepValid(current_step) === false) {
        // DONT'T GO TO NEXT STEP
        return;
    }
    let next_step = current_step + 1;
    //
    if (next_step == 2) {
        available_timeRender();
    }
    else if (next_step == 3) {
        if ($('input[name="id"]', booking_form).val() == "") {
            // Case 1 : not logged in
            showLogin();
            return false;
        }
        else if ($('input[name="address_id"]', booking_form).val() == "") {
            // Case 2 : no default address id found (may be because of no addrees in db)
            showNewAddress();
            toast('Add Address', 'Please add address to continue.', 'info');
            return false;
        }
    }
    //
    $('.step-' + current_step).hide();
    $('.step-' + next_step).show();
    // do after going to step
    /************************************* */
    let _meta_title_new = _meta_title + ' ' + next_step + '/3';
    $(document).prop('title', _meta_title_new);
    /************************************* */
    if (next_step == 3) {
        $('input[name="payment_method"]:checked', booking_form).trigger('change');
    }
    //
    window.history.pushState({}, "", decodeURI(_current_url + $.query.set('step', next_step)));
    window.scrollTo(0, 0);
});
$('[data-action="prev-step"]').click(function () {
    booking_form = $('#booking-form');
    let current_step = Number($(this).attr('data-step'));
    let prev_step = current_step - 1;
    //
    $('.step-' + current_step).hide();
    $('.step-' + prev_step).show();
    /************************************* */
    $(document).prop('title', _meta_title + ' ' + prev_step + '/3');
    /************************************* */
    window.history.pushState({}, "", decodeURI(_current_url + $.query.set('step', prev_step)));
    window.scrollTo(0, 0);
});
$(booking_form).submit(function () {
    // just to show last step error toasts
    isStepValid(3);
});
function isStepValid(step) {
    var required = [];
    if (step == 1) {
        required = [
            {
                name: 'packages[]',
                status: $('input[name="packages[]"]', booking_form).valid()
            },
        ];
    }
    else if (step == 2) {
        required = [
            {
                name: 'date',
                status: $('input[name="date"]', booking_form).valid()
            },
            {
                name: 'time',
                status: $('input[name="time"]', booking_form).valid()
            },
            {
                name: 'instructions',
                status: $('textarea[name="instructions"]', booking_form).valid()
            },
        ];
    }
    else if (step == 3) {
        required = [
            {
                name: 'payment_method',
                status: $('input[name="payment_method"]', booking_form).valid()
            },
        ];
    }
    /************************************************ */
    // check for errors and show as toast
    var errors = required.filter(field => {
        return field.status === false
    });
    if (errors.length > 0) {
        var first_error_field = errors[0].name;
        var error_message = booking_form_validator.submitted[first_error_field];
        toast(null, error_message, 'info');
        scrollToElement($('[name="' + first_error_field + '"]', booking_form));
        return false;
    }
    /************************************************ */
    // step valid
    return true;
}
function goToStep(step){
    $('div[class*=step-]').hide();
    $('.step-' + step).show();
    $('.step-back-icon').show(); // back button hiding issue fix
}
/*********************************************************************************************/
function packageToggled() {
    _cart.packages = [];
    let no_of_professionals = 0;
    $('input[name="packages[]"]:checked', booking_form).each(function (index, obj) {
        let quantity = $('#package_quanity_' + $(this).val()).val();
        no_of_professionals += $(this).attr('data-no_of_maids') * quantity;
        $('#booking-summary .professionals_count').html(no_of_professionals);
        var package = {
            package_id: $(this).val(),
            quantity: $('#package_quanity_' + $(this).val()).val(),
        }
        _cart.packages.push(package);
    });
    if (_cart.packages.length == 0) {
        toast(null, "Please select atleast one package.", 'info');
        goToStep(1);
    }
    calculate();
}
$('input[name="packages[]"]', booking_form).change(function () {
    if (this.checked) {
        $('input[type="number"][data-package_id="' + this.value + '"]').val(1);
        $('#package-summary-row-' + this.value).show();
    }
    else {
        $('input[type="number"][data-package_id="' + this.value + '"]').val(0);
        $('#package-summary-row-' + this.value).hide();
    }
    if (packageHours() > 12) {
        $('#package-summary-row-' + this.value).hide();
        $(this).prop('checked', false);
        toast("12 Hours exceeded", "Maximum work hour of 12 exceeded.", 'warning');
        return false;
    }
    packageToggled();
});
$('[data-action="package-plus"]', booking_form).click(function () {
    current_quantity = Number($('input[name="package_quanity[]"][data-package_id="' + $(this).attr("data-package_id") + '"]').val());
    max_quantity = $('input[name="package_quanity[]"][data-package_id="' + $(this).attr("data-package_id") + '"]').attr("max");
    if (current_quantity >= max_quantity) {
        $('input[name="package_quanity[]"][data-package_id="' + $(this).attr("data-package_id") + '"]').val(max_quantity);
        toast("Maximum selected", "Maximum of qty. " + max_quantity + " already selected.", 'info');
    }
    else {
        $('input[name="package_quanity[]"][data-package_id="' + $(this).attr("data-package_id") + '"]').val(current_quantity + 1);
    }
    if (packageHours() > 12) {
        $('input[name="package_quanity[]"][data-package_id="' + $(this).attr("data-package_id") + '"]').val(current_quantity);
        toast("12 Hours exceeded", "Maximum work hour of 12 exceeded.", 'warning');
        return false;
    }
    packageToggled();
});
$('[data-action="package-minus"]', booking_form).click(function () {
    current_quantity = Number($('input[name="package_quanity[]"][data-package_id="' + $(this).attr("data-package_id") + '"]').val());
    min_quantity = $('input[name="package_quanity[]"][data-package_id="' + $(this).attr("data-package_id") + '"]').attr("min");
    updated_quantity = current_quantity - 1;
    if (updated_quantity == 0) {
        $('#package-' + $(this).attr("data-package_id")).prop('checked', false);
        $('#package-summary-row-' + $(this).attr("data-package_id")).hide();
    }
    else if (updated_quantity > min_quantity) {
        $('input[name="package_quanity[]"][data-package_id="' + $(this).attr("data-package_id") + '"]').val(updated_quantity);
    }
    else {
        $('input[name="package_quanity[]"][data-package_id="' + $(this).attr("data-package_id") + '"]').val(min_quantity);
    }
    packageToggled();
});
/*********************************************************************************************/
var available_datetime_req = null;
function available_datetimeRender() {
    $('#times-holder').html(loading_html);
    booking_form = $('#booking-form');
    available_datetime_req = $.ajax({
        type: 'GET',
        url: _base_url + "api/customer/available_datetime",
        dataType: 'json',
        beforeSend: function () {
            if (available_datetime_req != null) {
                available_datetime_req.abort();
            }
        },
        success: function (response) {
            var dates_html = ``;
            $.each(response.result.available_dates, function (index, date) {
                let disabled = response.result.disabled_dates.find(disabled_date => {
                    return disabled_date == date
                });
                disabled = disabled ? 'disabled' : '';
                title = disabled ? 'Holiday' : '';
                dates_html += `<div class="item">
                    <input id="date-`+ index + `" value="` + moment(date, 'YYYY-MM-DD').format('DD/MM/YYYY') + `" name="date" class="" type="radio" ` + disabled + `>
                    <label for="date-`+ index + `" title="` + title + `">
                        <div class="calendar-tmb-main">
                            <div class="calendar-tmb-day">`+ moment(date, 'YYYY-MM-DD').format('ddd') + `</div>
                            <div class="calendar-tmb-date">`+ moment(date, 'YYYY-MM-DD').format('DD') + `</div>
                            <div class="calendar-tmb-month">`+ moment(date, 'YYYY-MM-DD').format('MMM') + `</div>
                        </div>
                    </label>
                </div>`;
            });
            $('#calendar').html(dates_html);
            $('#booking-form input[name="date"]').change(function () {
                var summary_date = moment(this.value, 'DD/MM/YYYY').format('DD MMM YYYY');
                $('#booking-summary .date').html(summary_date);
                available_timeRender();
                //calculate();
            });
            $('#calendar.owl-carousel').trigger('destroy.owl.carousel'); //these 3 lines kill the owl, and returns the markup to the initial state
            $('#calendar.owl-carousel').find('.owl-stage-outer').children().unwrap();
            $('#calendar.owl-carousel').removeClass("owl-center owl-loaded owl-text-select-on");
            $("#calendar.owl-carousel").owlCarousel({
                nav: true,
                loop: false,
                dots: false,
                margin: 15,
                autoplay: false,
                autoplayTimeout: 2000,
                smartSpeed: 800,
                autoplayHoverPause: false,
                responsive: {
                    0: {
                        items: 5
                    },
                    600: {
                        items: 5
                    },
                    1000: {
                        items: 10
                    },
                    1100: {
                        items: 10
                    },
                    1200: {
                        items: 10,
                        //margin: 50
                    }
                }
            }); //re-initialise the owl
            $('#booking-form input[name="date"]:first').trigger('click');
            renderTimes(response.result.available_times);
        },
        error: function (response) {
        },
    });
}
/*********************************************************************************************/
function renderTimes(available_times) {
    var times_html = ``;
    $.each(available_times, function (index, time) {
        times_html += `<li>
                    <input id="time-`+ index + `" value="` + time + `" name="time" class="" type="radio">
                    <label for="time-`+ index + `">
                        <!--<p>AED 5 Extra</p>-->`+ moment(time, "HH:mmm").format("hh:mm A") + `
                    </label>
                </li>`;
    });
    if (times_html == '') {
        times_html = `<div class="alert alert-info" role="alert">
                            <i class="fa fa-exclamation-circle" aria-hidden="true"></i> No time slots available for the selected date!
                    </div>`;
    }
    $('#times-holder').html(times_html);
    $('#booking-form input[name="time"]').change(function () {
        var hours = packageHours();
        $('input[name="hours"]', booking_form).val(hours); // set hours based on selected packages
        var summary_time_from = moment(this.value, 'H:mm').format('hh:mm A');
        var summary_time_to = moment(this.value, 'H:mm').add(hours, 'hours').format('hh:mm A');
        $('#booking-summary .time').html(summary_time_from + ` to ` + summary_time_to);
        calculate();
    });
    calculate();
    //$('#booking-form input[name="time"]:first').trigger('click');
}
/*********************************************************************************************/
function packageHours() {
    /**
     * find total working hours based selected packages
     * will return as hours
     * eg : 2.5
     * eg : 3
     */
    let total_seconds = 0;
    $('input[name="packages[]"]:checked', booking_form).each(function (index, obj) {
        // hours total based on packages
        let seconds = moment($(this).attr('data-service_time'), 'HH:mm:ss').diff(moment().startOf('day'), 'seconds');
        let quantity = $('#package_quanity_' + $(this).val()).val();
        total_seconds += seconds * quantity;
    });
    return total_seconds / 3600;
}
/*********************************************************************************************/
var available_time_req = null;
function available_timeRender() {
    $('#times-holder').html(loading_html);
    booking_form = $('#booking-form');
    available_time_req = $.ajax({
        type: 'GET',
        url: _base_url + "api/customer/available_time",
        data: {
            date: $('input[name="date"]:checked', booking_form).val(),
            packages: _cart.packages
        },
        dataType: 'json',
        //_cart.packages
        beforeSend: function () {
            if (available_time_req != null) {
                available_time_req.abort();
            }
        },
        success: function (response) {
            renderTimes(response.result.available_times);
        },
        error: function (response) {
        },
    });
}
/*********************************************************************************************/
$(document).ready(function () {
    var strategy = undefined;
    var match = document.location.search.match(/strategy=(.\S+)/);
    if (match) strategy = match[1];
    $(function () {
        $('.sticker').fixie({
            topMargin: 70,
            pinSlop: 0,
            strategy: strategy,
            //pinnedBodyClass: 'showGlobalChange'
        });
    });

    var owl = $("#package-category");
    owl.owlCarousel({
        items: 7,
        autoWidth: true,
        loop: false,
        nav: true,
        dots: false,
        margin: 10,
        autoplay: false,
        autoplayTimeout: 2000,
        smartSpeed: 800,
        autoplayHoverPause: false,
        responsive: {
            0: {
                items: 4
            },
            600: {
                items: 4
            },
            1000: {
                items: 5
            },
            1100: {
                items: 6
            },
            1200: {
                items: 6,
                //margin: 50
            }
        }
    });


});
/*********************************************************************************************/
$().ready(function () {
    if (_coupon_code != null) {
        _cart.coupon_code = _coupon_code;
    }
    if (Cookies.get('coupon_code')) {
        _cart.coupon_code = Cookies.get('coupon_code');
    }
    $(".scroll").jScroll();
    available_datetimeRender();
    create_booking_req = null;
    booking_form_validator = $('#booking-form').validate({
        focusInvalid: false,
        ignore: [],
        rules: {
            "packages[]": {
                required: true,
                minlength: 1
            },
            "date": {
                required: true,
            },
            "time": {
                required: true,
            },
            "instructions": {
                required: false,
            },
            "payment_method": {
                required: true,
            },
            "card_number": {
                required: 'input[name="payment_method"][value="2"]:checked'
            },
            "exp_date": {
                required: 'input[name="payment_method"][value="2"]:checked'
            },
            "cvv": {
                required: 'input[name="payment_method"][value="2"]:checked'
            },
        },
        messages: {
            "packages[]": {
                required: "Please select atleast one package.",
            },
            "date": {
                required: "Select service date",
            },
            "time": {
                required: "Select service time",
            },
            "instructions": {
                required: "Type any instructions",
            },
            "payment_method": {
                required: "Select payment method",
            },
            "card_number": {
                required: "Enter card number",
            },
            "exp_date": {
                required: "Enter card expiry date",
            },
            "cvv": {
                required: "Enter CVV code",
            },
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "packages[]") {
            }
            else if (element.attr("name") == "date") {
                error.insertAfter($('#calendar').parent());
            }
            else if (element.attr("name") == "time") {
                error.insertAfter($('#times-holder').parent());
            }
            else if (element.attr("name") == "hours") {
                error.insertAfter($('#hours-count-holder').parent());
            }
            else if (element.attr("name") == "professionals_count") {
                error.insertAfter($('#professionals-count-holder').parent());
            }
            else if (element.attr("name") == "cleaning_materials") {
                error.insertAfter($('#cleaning-materials-holder').parent());
            }
            else if (element.attr("name") == "instructions") {
                error.insertAfter($('#booking-form textarea[name="instructions"]').parent());
            }
            else if (element.attr("name") == "payment_method") {
                error.insertAfter($('#payment-method-holder').append());
            }
            else {
                error.insertAfter(element);
            }
            //formValidationError(null, error);
        },
        submitHandler: function (form) {
            booking_form = form;
            createBooking();
            return false;
        }
    });
});
/*********************************************************************************************/
debug = true;
function logit(data) {
    if (debug == true) {
        console.log(data);
    }
};
/*var merchantIdentifier = "merchant.com.emaid.elitmaidsUser.livennew";
if (window.ApplePaySession) {
    var promise = ApplePaySession.canMakePaymentsWithActiveCard(merchantIdentifier);
    promise.then(function (canMakePayments) {
        if (canMakePayments) {
            // Display Apple Pay button here.
            logit('hi, I can do ApplePay');
        }
    });
} else {
    logit('ApplePay is not available on this browser');
}
/*********************************************************************************************/
var session = null; // apple pay session store
function createBooking() {
    booking_form = $('#booking-form');
    let payment_method = $('input[name="payment_method"]:checked', booking_form).val();
    if (payment_method == 1) {
        // continue
    }
    else if (payment_method == 2) {
        if (_checkout_token_data) {
            // continue
        }
        else {
            checkoutTokenization();
            return false;
        }
    }
    else if (payment_method == 3) {
        // apple pay
        if (_checkout_token_data) {
            // continue
        }
        else {
            try {
                var runningAmount = _calculation_data.summary.total_payable;
                var runningPP = 0;
                getShippingCosts('domestic_std', true);
                var runningTotal = function () {
                    return runningAmount + runningPP;
                }
                var shippingOption = "";
                var subTotalDescr = "Payment against Cleaning Service";
                function getShippingOptions(shippingCountry) {
                    logit('getShippingOptions: ' + shippingCountry);
                    if (shippingCountry.toUpperCase() == "AE") {
                        shippingOption = [{
                            label: 'Standard Shipping',
                            amount: getShippingCosts('domestic_std', true),
                            detail: '3-5 days',
                            identifier: 'domestic_std'
                        }, {
                            label: 'Expedited Shipping',
                            amount: getShippingCosts('domestic_exp', false),
                            detail: '1-3 days',
                            identifier: 'domestic_exp'
                        }];
                    } else {
                        shippingOption = [{
                            label: 'International Shipping',
                            amount: getShippingCosts('international', true),
                            detail: '5-10 days',
                            identifier: 'international'
                        }];
                    }
                }
                function getShippingCosts(shippingIdentifier, updateRunningPP) {
                    var shippingCost = 0;
                    switch (shippingIdentifier) {
                        case 'domestic_std':
                            shippingCost = 0;
                            break;
                        case 'domestic_exp':
                            shippingCost = 0;
                            break;
                        case 'international':
                            shippingCost = 0;
                            break;
                        default:
                            shippingCost = 0;
                    }
                    if (updateRunningPP == true) {
                        runningPP = shippingCost;
                    }
                    logit('getShippingCosts: ' + shippingIdentifier + " - " + shippingCost + "|" + runningPP);
                    return shippingCost;
                }
                var paymentRequest = {
                    currencyCode: 'AED',
                    countryCode: 'AE',
                    requiredShippingContactFields: ['postalAddress'],
                    lineItems: [{
                        label: subTotalDescr,
                        amount: runningAmount
                    }, {
                        label: 'P&P',
                        amount: runningPP
                    }],
                    total: {
                        label: 'Elitemaids',
                        amount: runningTotal()
                    },
                    supportedNetworks: ['amex', 'masterCard', 'visa'],
                    merchantCapabilities: ['supports3DS', 'supportsCredit', 'supportsDebit']
                };
                session = new ApplePaySession(1, paymentRequest);
                // Merchant Validation
                session.onvalidatemerchant = function (event) {
                    logit(event);
                    var promise = performValidation(event.validationURL);
                    promise.then(function (merchantSession) {
                        session.completeMerchantValidation(merchantSession);
                    });
                }
                function performValidation(valURL) {
                    return new Promise(function (resolve, reject) {
                        var xhr = new XMLHttpRequest();
                        xhr.onload = function () {
                            var data = JSON.parse(this.responseText);
                            logit(data);
                            resolve(data);
                        };
                        xhr.onerror = reject;
                        xhr.open('GET', _base_url + 'applepay/apple_pay_comm.php?u=' + valURL);
                        xhr.send();
                    });
                }
                session.onshippingcontactselected = function (event) {
                    logit('starting session.onshippingcontactselected');
                    logit(
                        'NB: At this stage, apple only reveals the Country, Locality and 4 characters of the PostCode to protect the privacy of what is only a *prospective* customer at this point. This is enough for you to determine shipping costs, but not the full address of the customer.');
                    logit(event);
                    getShippingOptions(event.shippingContact.countryCode);
                    var status = ApplePaySession.STATUS_SUCCESS;
                    var newShippingMethods = shippingOption;
                    var newTotal = {
                        type: 'final',
                        label: 'Elitemaids',
                        amount: runningTotal()
                    };
                    var newLineItems = [{
                        type: 'final',
                        label: subTotalDescr,
                        amount: runningAmount
                    }, {
                        type: 'final',
                        label: 'P&P',
                        amount: runningPP
                    }];
                    session.completeShippingContactSelection(status, newShippingMethods, newTotal, newLineItems);
                }
                session.onshippingmethodselected = function (event) {
                    logit('starting session.onshippingmethodselected');
                    logit(event);
                    getShippingCosts(event.shippingMethod.identifier, true);
                    var status = ApplePaySession.STATUS_SUCCESS;
                    var newTotal = {
                        type: 'final',
                        label: 'Elitemaids',
                        amount: runningTotal()
                    };
                    var newLineItems = [{
                        type: 'final',
                        label: subTotalDescr,
                        amount: runningAmount
                    }, {
                        type: 'final',
                        label: 'P&P',
                        amount: runningPP
                    }];
                    session.completeShippingMethodSelection(status, newTotal, newLineItems);
                }
                session.onpaymentmethodselected = function (event) {
                    logit('starting session.onpaymentmethodselected');
                    logit(event);
                    var newTotal = {
                        type: 'final',
                        label: 'Elitemaids',
                        amount: runningTotal()
                    };
                    var newLineItems = [{
                        type: 'final',
                        label: subTotalDescr,
                        amount: runningAmount
                    }, {
                        type: 'final',
                        label: 'P&P',
                        amount: runningPP
                    }];
                    session.completePaymentMethodSelection(newTotal, newLineItems);
                }
                session.onpaymentauthorized = function (event) {
                    logit('starting session.onpaymentauthorized');
                    logit(
                        'NB: This is the first stage when you get the *full shipping address* of the customer, in the event.payment.shippingContact object');
                    logit(event.payment.token);
                    var eventToken = JSON.stringify(event.payment.token.paymentData);
                    _checkout_token_data = eventToken;
                    createBooking();
                }
                function sendPaymentToken(paymentToken) {
                    return new Promise(function (resolve, reject) {
                        logit('starting function sendPaymentToken()');
                        logit(
                            "this is where you would pass the payment token to your third-party payment provider to use the token to charge the card. Only if your provider tells you the payment was successful should you return a resolve(true) here. Otherwise reject;");
                        logit(
                            "defaulting to resolve(true) here, just to show what a successfully completed transaction flow looks like");
                        if (debug == true)
                            resolve(true);
                        else
                            reject;
                    });
                }
                session.oncancel = function (event) {
                }
                session.begin();
            }
            catch (err) {
                showTextAlert(JSON.stringify(err.message));
            }
            return false;
        }
    }
    else if (payment_method == 4) {
        booking_btn = $('button[type="button"].gpay-button', booking_form);
        booking_btn.attr('disabled', true);
        if (_checkout_token_data) {
            // continue
        }
        else {
            try {
                const baseRequest = {
                    apiVersion: 2,
                    apiVersionMinor: 0
                };
                const tokenizationSpecification = {
                    type: 'PAYMENT_GATEWAY',
                    parameters: {
                        'gateway': 'checkoutltd',
                        'gatewayMerchantId': _checkout_primary_key
                    }
                };
                const allowedCardNetworks = ["AMEX", "DISCOVER", "INTERAC", "JCB", "MASTERCARD", "VISA"];

                const allowedCardAuthMethods = ["PAN_ONLY", "CRYPTOGRAM_3DS"];

                const baseCardPaymentMethod = {
                    type: 'CARD',
                    parameters: {
                        allowedAuthMethods: allowedCardAuthMethods,
                        allowedCardNetworks: allowedCardNetworks
                    }
                };
                const cardPaymentMethod = Object.assign(
                    { tokenizationSpecification: tokenizationSpecification },
                    baseCardPaymentMethod
                );

                const paymentsClient = new google.payments.api.PaymentsClient({ environment: _google_pay_environment });

                const isReadyToPayRequest = Object.assign({}, baseRequest);
                isReadyToPayRequest.allowedPaymentMethods = [baseCardPaymentMethod];

                paymentsClient.isReadyToPay(isReadyToPayRequest)
                    .then(function (response) {
                        if (response.result) {
                            // add a Google Pay payment button
                        }
                    })
                    .catch(function (err) {
                        // show error in developer console for debugging
                        console.error(err);
                    });
                const paymentDataRequest = Object.assign({}, baseRequest);
                paymentDataRequest.allowedPaymentMethods = [cardPaymentMethod];
                paymentDataRequest.transactionInfo = {
                    totalPriceStatus: 'NOT_CURRENTLY_KNOWN',
                    //totalPriceStatus: 'FINAL',
                    //totalPrice: _calculation_data.summary.total_payable.toString(),
                    currencyCode: 'AED',
                    countryCode: 'AE'
                };
                paymentDataRequest.merchantInfo = {
                    merchantName: 'Elite Advance Building Cleaning LLC',
                    merchantId: 'BCR2DN4TXLQ3FHS6'
                };

                paymentsClient.loadPaymentData(paymentDataRequest).then(function (paymentData) {
                    // if using gateway tokenization, pass this token without modification
                    paymentToken = paymentData.paymentMethodData.tokenizationData.token;
                    _checkout_token_data = paymentToken;
                    createBooking();
                    // continue
                }).catch(function (err) {
                    // show error in developer console for debugging
                    console.error(err);
                    booking_btn.attr('disabled', false);
                });
                //paymentsClient.prefetchPaymentData(paymentDataRequest);
            }
            catch (err) {
                showTextAlert(err.message);
            }
            return false
        }
    }
    else if (payment_method == 5) {
        // continue
    }
    else if (payment_method == 6) {
        // continue
    }
    else if (payment_method == 7) {
        // continue
    }
    booking_btn = $('button[type="submit"]', booking_form);
    booking_btn.html(loading_button_html);
    booking_btn.attr('disabled', true);
    create_booking_req = $.ajax({
        type: 'POST',
        url: _base_url + "api/customer/create_booking",
        dataType: 'json',
        data: {
            address_id: $('input[name="address_id"]', booking_form).val(),
            service_type_id: $('input[name="service_type_id"]', booking_form).val(),
            professionals_count: $('input[name="professionals_count"]:checked', booking_form).val(),
            cleaning_materials: 0,
            addons: _cart.addons,
            packages: _cart.packages,
            frequency: $('input[name="frequency"]:checked', booking_form).val(),
            coupon_code: _cart.coupon_code,
            date: $('input[name="date"]:checked', booking_form).val(),
            time: $('input[name="time"]:checked', booking_form).val(),
            payment_method: $('input[name="payment_method"]:checked', booking_form).val(),
            instructions: $('textarea[name="instructions"]', booking_form).val(),
            checkout_token_data: _checkout_token_data
        },
        beforeSend: function () {
            if (create_booking_req != null) {
                create_booking_req.abort();
            }
        },
        success: function (response) {
            if (response.result.status == "success") {
                if (_calculation_data.input.payment_method == 1) {
                    booking_btn.attr('disabled', false);
                    booking_btn.html('Completed');
                    Swal.fire({
                        title: "Booking Received !",
                        html: "Booking received with Ref. No. <b>" + response.result.booking_details.booking_reference_id + '</b>',
                        icon: "success",
                        allowOutsideClick: false,
                        confirmButtonText: 'Show Details',
                        timer: 3000,
                    }).then((result) => {
                        if (result.isConfirmed || result.dismiss === swal.DismissReason.timer) {
                            window.location.href = _base_url + 'booking/success/' + response.result.booking_details.booking_reference_id;
                        }
                    });
                }
                else if (_calculation_data.input.payment_method == 2) {
                    let checkout_data = response.result.checkout_data;
                    if (checkout_data.status.toLowerCase() == "pending") {
                        // redirect to bank otp page
                        window.location.href = checkout_data._links.redirect.href;
                    }
                    else if (checkout_data.approved == true) {
                        // payment success
                        booking_btn.html('Completed');
                        Swal.fire({
                            title: "Payment Received !",
                            html: "Booking received with Ref. No. <b>" + response.result.booking_details.booking_reference_id + '</b>',
                            icon: "success",
                            allowOutsideClick: false,
                            confirmButtonText: 'Show Details',
                            timer: 3000,
                        }).then((result) => {
                            if (result.isConfirmed || result.dismiss === swal.DismissReason.timer) {
                                window.location.href = _base_url + 'booking/success/' + response.result.booking_details.booking_reference_id;
                            }
                        });
                    }
                }
                else if (_calculation_data.input.payment_method == 3) {
                    let checkout_data = response.result.checkout_data;
                    if (checkout_data.status.toLowerCase() == "pending") {
                        // redirect to bank otp page
                        window.location.href = checkout_data._links.redirect.href;
                    }
                    else if (checkout_data.approved == true) {
                        // payment success
                        session.completePayment(ApplePaySession.STATUS_SUCCESS);
                        booking_btn.html('Completed');
                        Swal.fire({
                            title: "Payment Received !",
                            html: "Booking received with Ref. No. <b>" + response.result.booking_details.booking_reference_id + '</b>',
                            icon: "success",
                            allowOutsideClick: false,
                            confirmButtonText: 'Show Details',
                            timer: 3000,
                        }).then((result) => {
                            if (result.isConfirmed || result.dismiss === swal.DismissReason.timer) {
                                window.location.href = _base_url + 'booking/success/' + response.result.booking_details.booking_reference_id;
                            }
                        });
                    }
                }
                else if (_calculation_data.input.payment_method == 4) {
                    let checkout_data = response.result.checkout_data;
                    if (checkout_data.status.toLowerCase() == "pending") {
                        // redirect to bank otp page
                        window.location.href = checkout_data._links.redirect.href;
                    }
                    else if (checkout_data.approved == true) {
                        // payment success
                        booking_btn.html('Completed');
                        Swal.fire({
                            title: "Payment Received !",
                            html: "Booking received with Ref. No. <b>" + response.result.booking_details.booking_reference_id + '</b>',
                            icon: "success",
                            allowOutsideClick: false,
                            confirmButtonText: 'Show Details',
                            timer: 3000,
                        }).then((result) => {
                            if (result.isConfirmed || result.dismiss === swal.DismissReason.timer) {
                                window.location.href = _base_url + 'booking/success/' + response.result.booking_details.booking_reference_id;
                            }
                        });
                    }
                }
                else if (_calculation_data.input.payment_method == 5) {
                    let checkout_data = response.result.checkout_data;
                    if (checkout_data.order_id !== undefined) {
                        // redirect to tamara
                        window.location.href = checkout_data.checkout_url;
                    }
                }
                else if (_calculation_data.input.payment_method == 6) {
                    // Card (Telr)
                    let telr_data = response.result.telr_data;
                    if (telr_data.order.url !== undefined) {
                        // redirect to tamara
                        //showTelrFrame(telr_data.order.url);
                        window.location.href = telr_data.order.url;
                    }
                }
                else if (_calculation_data.input.payment_method == 7) {
                    // Card (Applepay)
                    let telr_data = response.result.telr_data;
                    if (telr_data.order.url !== undefined) {
                        // redirect to tamara
                        //showTelrFrame(telr_data.order.url);
                        window.location.href = telr_data.order.url;
                    }
                }
            }
            else {
                if (_calculation_data.input.payment_method == 3) {
                    session.completePayment(ApplePaySession.STATUS_FAILURE);
                }
                _checkout_token_data = undefined;
                booking_btn.attr('disabled', false);
                booking_btn.html('Complete');
                create_booking_error(response);
            }
        },
        error: function (response) {
            ajaxError(null, response);
            //booking_btn.attr('disabled', false);
        },
    });
}

$('.show-pak-det-popup').click(function () {
    let package = JSON.parse($(this).closest('.booking-packages').find("input[name='package[]']").val());
    let div = $(this).closest('.booking-packages');
    showPackagePopup(package);
    // $('.packages-details-popup').show( 500);
});

function showPackagePopup(package) {
    // console.log(package)
    $('.package_tumbnail').html('<img src="' + package.thumbnail_url + '" alt=""></img>');
    $('.package_details_name').html(package.package_name);
    $('.package_details_description').html(package.package_description);
    $('.package_amount').html('AED <span>' + package.actual_amount + '</span>  ' + package.amount);
    $('.info_html').html(package.info_html);
    $('.packages-details-popup').show(500);
}

$('.pak-det-popup-bot-btn').click(function () {
    $('.packages-details-popup').hide(500);
});

$('.popup-close, .close-popup').click(function () {
    $('.popup-main').hide(500);
});