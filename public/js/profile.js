$('#profile-address-list-holder').html(loading_html);
$(document).ready(function () {
    fetchAddressList();
    default_address_form_validator = $('#default_address_form').validate({
        focusInvalid: false,
        ignore: [],
        rules: {
            "address_id": {
                required: true,
            }
        },
        messages: {
            "address_id": {
                required: "Select an address",
            }
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        },
        submitHandler: function (form) {
            let submit_btn = $('button[type="submit"]', form);
            submit_btn.html(loading_button_html).prop("disabled", true);
            $.ajax({
                type: 'POST',
                url: _base_url + "api/customer/set_default_address",
                dataType: 'json',
                data: $('#default_address_form').serialize(),
                success: function (response) {
                    submit_btn.html('Set Default Address').prop("disabled", false);
                    if (response.result.status == "success") {
                        toast('Success', response.result.message, 'info');
                        fetchAddressList();
                    } else {
                    }
                },
                error: function (response) {
                    submit_btn.html('Set Default Address').prop("disabled", false);
                },
            });
        }
    });
});