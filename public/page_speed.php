<?php
$starttime = microtime(true); // Top of page
// Code
$endtime = microtime(true); // Bottom of page

printf("Page loaded in %f seconds", $endtime - $starttime);