	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Emaid Booking</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="theme-color" content="#0c3995">
<link rel="icon" type="image/png" href="images/favicon.png"/>

<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" >
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" href="css/animation.css"/>
<link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">

</head>

<body>

<div class="wrapper-main">
	
    
    <?php require_once('include/header.php') ?>
	
    
    <section>
		<div class="container">
			<div class="row inner-wrapper m-0">
			
			 <div class="col-sm-12 top-steps-section">
				  <div class="d-flex page-title-section">
					  <div class="booking-page-title flex-grow-1"><h3>Popular Add-ons</h3></div>
					  <div class="step-back-icon"><a href="booking-service-details.php" class="back-arrow" title="Click to Back">Step 2</a></div>
					  <div class="booking-steps"> of 4</div>
				  </div>
			 </div>
					  
					  
				 
				 <div class="col-lg-8 col-md-7 booking-form-left">
				      
					  
					  <div class="col-sm-12 add-ons-wrapper">
					       <div class="col-sm-12 p-0 pb-2">
								<h4>People also added</h4>
					       </div>
					       <div class="col-sm-12 add-ons-scroll-section">
						   
						        <div id="add-ons" class="owl-carousel owl-theme p-0">
					                 <div class="item">
									      <input id="add-ons1" value="cash" name="add-ons" checked="checked" class="" type="checkbox">
                                          <label for="add-ons1">
												<div class="add-ons-scroll-thumb">
													 <div class="add-ons-scroll-image"><img src="images/add-ons1.jpg" alt="" /></div>
													 <div class="add-ons-scroll-cont-main">
														  <div class="add-ons-scroll-cont">
															   <h4>Cupboard Cleaning</h4>
															   <p><strong>Messy cupboards?</strong>
															   Lorem Ipsum is simply dummy teof the printing... <a href="#">Learn More</a></p>
														  </div>
														  <div class="add-ons-scroll-price">AED <span> 55 </span> 40</div>
														  <div class="add-ons-scroll-btn">
															   <div class="addon-btn-main">
																    <a class="sp-btn">Add</a>
															   </div>
															   <div class="addon-btn-count">
																    <input value="" class="addon-btn-minus" type="submit">
																    <input name="" value="1" class="addon-text-field" type="text">
																    <input value="" class="addon-btn-plus" type="submit">
															   </div>
														  </div>
													 </div>
												</div>
										  </label>	
									 </div>
									 
									 
									 <div class="item">
									      <input id="add-ons2" value="cash" name="add-ons" class="" type="checkbox">
                                          <label for="add-ons2">
												<div class="add-ons-scroll-thumb">
													 <div class="add-ons-scroll-image"><img src="images/add-ons1.jpg" alt="" /></div>
													 <div class="add-ons-scroll-cont-main">
														  <div class="add-ons-scroll-cont">
															   <h4>Cupboard Cleaning</h4>
															   <p><strong>Messy cupboards?</strong>
															   Lorem Ipsum is simply dummy teof the printing... <a href="#">Learn More</a></p>
														  </div>
														  <div class="add-ons-scroll-price">AED <span> 55 </span> 40</div>
														  <div class="add-ons-scroll-btn">
															   <div class="addon-btn-main">
																    <a class="sp-btn">Add</a>
															   </div>
															   <div class="addon-btn-count">
																    <input value="" class="addon-btn-minus" type="submit">
																    <input name="" value="1" class="addon-text-field" type="text">
																    <input value="" class="addon-btn-plus" type="submit">
															   </div>
														  </div>
													 </div>
												</div>
										  </label>	
									 </div>
									 
									 
									 <div class="item">
									      <input id="add-ons3" value="cash" name="add-ons" class="" type="checkbox">
                                          <label for="add-ons3">
												<div class="add-ons-scroll-thumb">
													 <div class="add-ons-scroll-image"><img src="images/add-ons1.jpg" alt="" /></div>
													 <div class="add-ons-scroll-cont-main">
														  <div class="add-ons-scroll-cont">
															   <h4>Cupboard Cleaning</h4>
															   <p><strong>Messy cupboards?</strong>
															   Lorem Ipsum is simply dummy teof the printing... <a href="#">Learn More</a></p>
														  </div>
														  <div class="add-ons-scroll-price">AED <span> 55 </span> 40</div>
														  <div class="add-ons-scroll-btn">
															   <div class="addon-btn-main">
																    <a class="sp-btn">Add</a>
															   </div>
															   <div class="addon-btn-count">
																    <input value="" class="addon-btn-minus" type="submit">
																    <input name="" value="1" class="addon-text-field" type="text">
																    <input value="" class="addon-btn-plus" type="submit">
															   </div>
														  </div>
													 </div>
												</div>
										  </label>	
									 </div>
									 
									 
									 <div class="item">
									      <input id="add-ons4" value="cash" name="add-ons" class="" type="checkbox">
                                          <label for="add-ons4">
												<div class="add-ons-scroll-thumb">
													 <div class="add-ons-scroll-image"><img src="images/add-ons1.jpg" alt="" /></div>
													 <div class="add-ons-scroll-cont-main">
														  <div class="add-ons-scroll-cont">
															   <h4>Cupboard Cleaning</h4>
															   <p><strong>Messy cupboards?</strong>
															   Lorem Ipsum is simply dummy teof the printing... <a href="#">Learn More</a></p>
														  </div>
														  <div class="add-ons-scroll-price">AED <span> 55 </span> 40</div>
														  <div class="add-ons-scroll-btn">
															   <div class="addon-btn-main">
																    <a class="sp-btn">Add</a>
															   </div>
															   <div class="addon-btn-count">
																    <input value="" class="addon-btn-minus" type="submit">
																    <input name="" value="1" class="addon-text-field" type="text">
																    <input value="" class="addon-btn-plus" type="submit">
															   </div>
														  </div>
													 </div>
												</div>
										  </label>	
									 </div>
									 
									 
									 <div class="item">
									      <input id="add-ons5" value="cash" name="add-ons" class="" type="checkbox">
                                          <label for="add-ons5">
												<div class="add-ons-scroll-thumb">
													 <div class="add-ons-scroll-image"><img src="images/add-ons1.jpg" alt="" /></div>
													 <div class="add-ons-scroll-cont-main">
														  <div class="add-ons-scroll-cont">
															   <h4>Cupboard Cleaning</h4>
															   <p><strong>Messy cupboards?</strong>
															   Lorem Ipsum is simply dummy teof the printing... <a href="#">Learn More</a></p>
														  </div>
														  <div class="add-ons-scroll-price">AED <span> 55 </span> 40</div>
														  <div class="add-ons-scroll-btn">
															   <div class="addon-btn-main">
																    <a class="sp-btn">Add</a>
															   </div>
															   <div class="addon-btn-count">
																    <input value="" class="addon-btn-minus" type="submit">
																    <input name="" value="1" class="addon-text-field" type="text">
																    <input value="" class="addon-btn-plus" type="submit">
															   </div>
														  </div>
													 </div>
												</div>
										  </label>	
									 </div>
								</div>
					            
							   
								
					       </div>
					  </div>
					  
				<div class="col-sm-12 booking-main-btn-section">
				 <div class="row m-0">
						   <div class="col-lg-5 col-md-4 col-sm-6 p-0 pt-3">
						        <a href="javascript:void(0);" class="frequency-popup-btn">
							    <input value="Next" class="text-field-btn" type="submit">
								</a>
						   </div>
					  </div>
				</div>
				 
				 </div>
				 
				 
				 <div class="col-lg-4 col-md-5 booking-summary-section">
				      <div class="col-lg-11 col-md-12 booking-summary-main clearfix scroll">
					  <div class="row m-0">
					       <div class="col-sm-12 book-details-main pb-2">
                                <h3>Booking Summary</h3>
						   </div>
                                      
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-6 book-det-left ps-0 pe-0"><p>Service</p></div>
									 <div class="col-6 book-det-right ps-0 pe-0"><p>Home Cleaning</p></div>
								</div>
						   </div> 
						   
						   
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-6 book-det-left ps-0 pe-0"><p>Service Details</p></div>
									 <div class="col-6 book-det-right ps-0 pe-0"><p>3x Cupboard Cleaning</p></div>
								</div>
						   </div> 
                                      
						   
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-6 book-det-left ps-0 pe-0"><p>Frequency</p></div>
									 <div class="col-6 book-det-right ps-0 pe-0"><p>One Time Service</p></div>
								</div>
						   </div>
						   
						   
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-6 book-det-left ps-0 pe-0"><p>Duration</p></div>
									 <div class="col-6 book-det-right ps-0 pe-0"><p>2 hours</p></div>
								</div>
						   </div>
						   
						   
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-5 book-det-left ps-0 pe-0"><p>Date & Time</p></div>
									 <div class="col-7 book-det-right ps-0 pe-0"><p>27 Sept 2023,<br />05:00 pm  to  07:00 pm</p></div>
								</div>
						   </div>
						   
						   
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-7 book-det-left ps-0 pe-0"><p>Number of Professionals</p></div>
									 <div class="col-5 book-det-right ps-0 pe-0"><p>5</p></div>
								</div>
						   </div>
						   
						   
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-6 book-det-left ps-0 pe-0"><p>Material</p></div>
									 <div class="col-6 book-det-right ps-0 pe-0"><p>No</p></div>
								</div>
						   </div>
						   
						   
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-5 book-det-left ps-0 pe-0"><p>Crew</p></div>
									 <div class="col-7 book-det-right ps-0 pe-0"><p>Elizabeth Delya</p></div>
								</div>
						   </div>  
						   
						   </div>
						   
						   
						   <div class="row book-details-main-set m-0 pt-5">
						   
							    <div class="col-sm-12 book-details-main pb-2">
                                     <h3>Payment Summary</h3>
						        </div>
								

								
								<div class="col-sm-12 book-details-main">
								  <div class="row booking-amount m-0">
									  <div class="col-6 book-det-left ps-0 pe-0"><p>Service Fee</p></div>
									  <div class="col-6 book-det-right ps-0 pe-0"><p><span>AED</span> 93.33</p></div>
								  </div>
								</div>
							  
							  
								<div class="col-sm-12 book-details-main">
								  <div class="row booking-amount m-0">
									  <div class="col-6 book-det-left ps-0 pe-0"><p>Total (inc VAT 5%)</p></div>
									  <div class="col-6 book-det-right ps-0 pe-0"><p><span>AED</span> 4.67</p></div>
								  </div>
								</div>
							  
							  
							  
								<div class="col-sm-12 book-details-main">
								  <div class="row total-price m-0">
									  <div class="col-5 book-det-left ps-0 pe-0"><p>Total</p></div>
									  <div class="col-7 book-det-right ps-0 pe-0"><p><span>AED</span> 98.00</p></div>
								  </div>
								</div>
								
						  </div>     
                      </div>
				 
				 </div>
				 
				 
				 <!--<div class="col-sm-12 booking-main-btn-section">
				 <div class="row m-0">
						   <div class="col-lg-3 col-md-4 col-sm-6 p-0 pt-3">
						        <a href="service-details.php">
							    <input value="Next" class="text-field-btn" type="submit">
								</a>
						   </div>
					  </div>
				</div>-->
				 
			</div>
		</div>  
    </section>

</div>

<?php require_once('include/footer.php') ?>
          
</body>
</html>
