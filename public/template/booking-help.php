	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Emaid Booking</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="theme-color" content="#0c3995">
<link rel="icon" type="image/png" href="images/favicon.png"/>

<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" >
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" href="css/animation.css"/>
<link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">

</head>

<body>

<div class="wrapper-main">
	
    
    <?php require_once('include/header.php') ?>
	
    
    <section>
		<div class="container">
			<div class="row inner-wrapper m-0">
                 <div class="col-sm-12 my-account-wrapper">


					  
					  <div class="col-sm-12">
					       <div class="d-flex">
							  <div class="step-back-icon"><a href="my-account.php" class="back-arrow" title="Click to Back"></a></div>
							  <div class="booking-page-title flex-grow-1"><h3>Booking Help</h3></div>
							  <div class="booking-back"><a href="index.php">Book Again</a></div>
							</div>
					  </div>
					  
					  
					  <div class="row m-0 pt-4">
						   
						   <div class="accordion p-0" id="accordionExample">
				
								<div class="accordion-item">
									<h2 class="accordion-header">
										<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
										<label>1,</label>
										I want to review my cancellation fees
										</button>
									</h2>
									<div id="collapse1" class="accordion-collapse collapse" data-bs-parent="#accordionExample">
										<div class="accordion-body"> <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p></div>
									</div>
								</div>
								
								
								<div class="accordion-item">
									<h2 class="accordion-header">
										<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse2" aria-expanded="true" aria-controls="collapse2">
										<label>2,</label>
										I want to receive my refund
										</button>
									</h2>
									<div id="collapse2" class="accordion-collapse collapse" data-bs-parent="#accordionExample">
										<div class="accordion-body"> <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p></div>
									</div>
								</div>
								
								
								<div class="accordion-item">
									<h2 class="accordion-header">
										<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse3" aria-expanded="true" aria-controls="collapse3">
										<label>3,</label>
										I didn’t receive my refund
										</button>
									</h2>
									<div id="collapse3" class="accordion-collapse collapse" data-bs-parent="#accordionExample">
										<div class="accordion-body"> <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy.</p></div>
									</div>
								</div>
								
								
								<div class="accordion-item">
									<h2 class="accordion-header">
										<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse4" aria-expanded="true" aria-controls="collapse4">
										<label>4,</label>
										Terms and Conditions
										</button>
									</h2>
									<div id="collapse4" class="accordion-collapse collapse" data-bs-parent="#accordionExample">
										<div class="accordion-body"><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting.</p></div>
									</div>
								</div>
								
								
								<div class="accordion-item">
									<h2 class="accordion-header">
										<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse5" aria-expanded="true" aria-controls="collapse5">
										<label>5,</label>
										Privacy Policy
										</button>
									</h2>
									<div id="collapse5" class="accordion-collapse collapse" data-bs-parent="#accordionExample">
										<div class="accordion-body"><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p></div>
									</div>
								</div>

					
				          </div>
						  
						  
					  </div>
					  
					  
					  
					  	
					 
					 
					 
					 
							
									
				 </div>
			</div>
		</div>  
    </section>

</div>

<?php require_once('include/footer.php') ?>
          
</body>
</html>
