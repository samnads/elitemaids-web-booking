	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Emaid Booking</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="theme-color" content="#0c3995">
<link rel="icon" type="image/png" href="images/favicon.png"/>

<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" >
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" href="css/animation.css"/>
<link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">

</head>

<body>

<div class="wrapper-main">
	
    
    <?php require_once('include/header.php') ?>
	
    
    <section>
		<div class="container">
			<div class="row inner-wrapper m-0">
			
			     <div class="col-sm-12 top-steps-section">
					       <div class="d-flex page-title-section">
							  
							  <div class="booking-page-title flex-grow-1"><h3>Service Details</h3></div>
							  <div class="step-back-icon"><a href="index.php" class="back-arrow" title="Click to Back">Step 1</a></div>
							  <div class="booking-steps"> of 4</div>
							</div>
					  </div>
					  
					  
				 
				 <div class="col-lg-8 col-md-7 booking-form-left">
				 
					  
					  <div class="col-sm-12 how-many-hours-wrapper">
					       <div class="col-sm-12 p-0 pb-2">
					            <h4>How many hours do you need your professional to stay?</h4>
					       </div>
					       <div class="col-sm-12 booking-form-list p-0">
					            <ul>
								    <li><input id="how-many-hours1" value="cash" name="how-many-hours" class="" type="radio">
                                        <label for="how-many-hours1">1</label>
									</li>
									
									
									<li><input id="how-many-hours2" value="cash" name="how-many-hours" class="" type="radio">
                                        <label for="how-many-hours2">2</label>
									</li>
									
									
									<li><input id="how-many-hours3" value="cash" name="how-many-hours" class="" type="radio">
                                        <label for="how-many-hours3">3</label>
									</li>
									
									
									<li><input id="how-many-hours4" value="cash" name="how-many-hours" class="" type="radio">
                                        <label for="how-many-hours4">4</label>
									</li>
									
									
									<li><input id="how-many-hours5" value="cash" name="how-many-hours" class="" type="radio">
                                        <label for="how-many-hours5">5</label>
									</li>
								</ul>
					       </div>
					  </div>
					  
					  
					  
					  <div class="col-sm-12 how-mainy-housekeepers-wrapper">
					       <div class="col-sm-12 p-0 pb-2">
					            <h4>How many professionals do you need?</h4>
					       </div>
					       <div class="col-sm-12 booking-form-list p-0">
					            <ul>
								    <li><input id="how-many-maids1" value="cash" name="how-many-maids" class="" type="radio">
                                        <label for="how-many-maids1">1</label>
									</li>
									
									<li><input id="how-many-maids2" value="cash" name="how-many-maids" class="" type="radio">
                                        <label for="how-many-maids2">2</label>
									</li>
									
									<li><input id="how-many-maids3" value="cash" name="how-many-maids" class="" type="radio">
                                        <label for="how-many-maids3">3</label>
									</li>
									
									<li><input id="how-many-maids4" value="cash" name="how-many-maids" class="" type="radio">
                                        <label for="how-many-maids4">4</label>
									</li>
									
									<li><input id="how-many-maids5" value="cash" name="how-many-maids" class="" type="radio">
                                        <label for="how-many-maids5">5</label>
									</li>
								</ul>
					       </div>
					  </div>
					  
					  
					  
					  <div class="col-sm-12 cleaning-materials-wrapper">
					       <div class="col-sm-12 p-0 pb-2">
					            <h4>Need cleaning materials?</h4>
					       </div>
					       <div class="col-sm-12 booking-form-list p-0">
					            <ul>
								    <li><input id="cleaning-materials1" value="cash" name="cleaning-materials" class="" type="radio">
                                        <label for="cleaning-materials1">No, I have them</label>
									</li>
									
									<li><input id="cleaning-materials2" value="cash" name="cleaning-materials" class="" type="radio">
                                        <label for="cleaning-materials2">Yes, please</label>
									</li>
								</ul>
					       </div>
					  </div>
					  
					  
					  
					  <div class="col-sm-12 cleaning-materials-wrapper">
					       <div class="col-sm-12 p-0 pb-2">
					            <h4>Any instructions or special requirements?</h4>
					       </div>
					       <div class="col-sm-12 booking-form-field p-0">
					            <textarea name="" cols="" rows="" class="text-field-big" spellcheck="false" placeholder="Write here..."></textarea>
					       </div>
					  </div>
					  

				 </div>
				 
				 
				 <div class="col-lg-4 col-md-5 booking-summary-section">
				      <div class="col-lg-11 col-md-12 booking-summary-main clearfix scroll">
					  <div class="row m-0">
					       <div class="col-sm-12 book-details-main pb-2">
                                <h3>Booking Summary</h3>
						   </div>
                                      
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-6 book-det-left ps-0 pe-0"><p>Service</p></div>
									 <div class="col-6 book-det-right ps-0 pe-0"><p>Home Cleaning</p></div>
								</div>
						   </div> 
						   
						   
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-6 book-det-left ps-0 pe-0"><p>Service Details</p></div>
									 <div class="col-6 book-det-right ps-0 pe-0"><p>3x Cupboard Cleaning</p></div>
								</div>
						   </div> 
                                      
						   
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-6 book-det-left ps-0 pe-0"><p>Frequency</p></div>
									 <div class="col-6 book-det-right ps-0 pe-0"><p>One Time Service</p></div>
								</div>
						   </div>
						   
						   
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-6 book-det-left ps-0 pe-0"><p>Duration</p></div>
									 <div class="col-6 book-det-right ps-0 pe-0"><p>2 hours</p></div>
								</div>
						   </div>
						   
						   
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-5 book-det-left ps-0 pe-0"><p>Date & Time</p></div>
									 <div class="col-7 book-det-right ps-0 pe-0"><p>27 Sept 2023,<br />05:00 pm  to  07:00 pm</p></div>
								</div>
						   </div>
						   
						   
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-7 book-det-left ps-0 pe-0"><p>Number of Professionals</p></div>
									 <div class="col-5 book-det-right ps-0 pe-0"><p>5</p></div>
								</div>
						   </div>
						   
						   
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-6 book-det-left ps-0 pe-0"><p>Material</p></div>
									 <div class="col-6 book-det-right ps-0 pe-0"><p>No</p></div>
								</div>
						   </div>
						   
						   
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-5 book-det-left ps-0 pe-0"><p>Crew</p></div>
									 <div class="col-7 book-det-right ps-0 pe-0"><p>Elizabeth Delya</p></div>
								</div>
						   </div>  
						   
						   </div>
						   
						   
						   <div class="row book-details-main-set m-0 pt-5">
						   
							    <div class="col-sm-12 book-details-main pb-2">
                                     <h3>Payment Summary</h3>
						        </div>
								

								
								<div class="col-sm-12 book-details-main">
								  <div class="row booking-amount m-0">
									  <div class="col-6 book-det-left ps-0 pe-0"><p>Service Fee</p></div>
									  <div class="col-6 book-det-right ps-0 pe-0"><p><span>AED</span> 93.33</p></div>
								  </div>
								</div>
							  
							  
								<div class="col-sm-12 book-details-main">
								  <div class="row booking-amount m-0">
									  <div class="col-6 book-det-left ps-0 pe-0"><p>Total (inc VAT 5%)</p></div>
									  <div class="col-6 book-det-right ps-0 pe-0"><p><span>AED</span> 4.67</p></div>
								  </div>
								</div>
							  
							  
							  
								<div class="col-sm-12 book-details-main">
								  <div class="row total-price m-0">
									  <div class="col-5 book-det-left ps-0 pe-0"><p>Total</p></div>
									  <div class="col-7 book-det-right ps-0 pe-0"><p><span>AED</span> 98.00</p></div>
								  </div>
								</div>
								
						  </div>     
                      </div>
				 
				 </div>
				 
				 <div class="col-sm-12 booking-main-btn-section">
				 <div class="row m-0">
						   <div class="col-lg-3 col-md-4 col-sm-6 p-0 pt-3">
						        <a href="booking-add-ons.php">
							    <input value="Next" class="text-field-btn" type="submit">
								</a>
						   </div>
					  </div>
				</div>
				 
				 
			</div>
		</div>  
    </section>

</div>

<?php require_once('include/footer.php') ?>
          
</body>
</html>
