	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Emaid Booking</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="theme-color" content="#0c3995">
<link rel="icon" type="image/png" href="images/favicon.png"/>

<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" >
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" href="css/animation.css"/>
<link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">

</head>

<body>

<div class="wrapper-main">
	
    
    <?php require_once('include/header.php') ?>
	
    
    <section>
		<div class="container">
			<div class="row inner-wrapper m-0">
                 <div class="col-sm-12 my-account-wrapper">


					  
					  <div class="col-sm-12">
					       <div class="d-flex">
							  <div class="step-back-icon"><a href="past-bookings.php" class="back-arrow" title="Click to Back"></a></div>
							  <div class="booking-page-title flex-grow-1"><h3>Cancelled Booking</h3></div>
							  <div class="booking-back"><a href="index.php">Book Again</a></div>
							</div>
					  </div>
					  
					  
					  <div class="row m-0 pt-5">
					       <div class="col-sm-12 p-0">
						        <h4 class="text-red">Booking Cancelled !</h4> 
						        <p>Your booking dated <strong>Oct 16</strong> has been cancelled successfully.</p>
						   </div>
						   
						   <div class="col-sm-12 maids-section clearfix p-0 pt-2">
							   <div class="pb-maids"><img src="images/maid.jpg" alt=""></div>
							   <div class="pb-maids"><img src="images/maid.jpg" alt=""></div>
							   <div class="pb-maids"><img src="images/maid.jpg" alt=""></div>
						  </div>
					  </div>
					  
					  
					  <div class="row cancelled-booking-details">
					       <div class="col-lg-4 col-md-6 booking-success-left">
								<div class="col-sm-12 book-details-main pb-2">
									<h4>Personal Details</h4>
								</div>
								
								<div class="col-sm-12 book-details-main">
									<div class="row m-0">
										<div class="col-6 book-det-left ps-0 pe-0">
											<p>Name</p>
										</div>
										<div class="col-6 book-det-right ps-0 pe-0">
											<p>Mathew</p>
										</div>
									</div>
								</div>
								
								<div class="col-sm-12 book-details-main">
									<div class="row m-0">
										<div class="col-6 book-det-left ps-0 pe-0">
											<p>Email ID</p>
										</div>
										<div class="col-6 book-det-right ps-0 pe-0">
											<p>mathew@gmail.com</p>
										</div>
									</div>
								</div>
								
								<div class="col-sm-12 book-details-main">
									<div class="row m-0">
										<div class="col-6 book-det-left ps-0 pe-0">
											<p>Contact Number</p>
										</div>
										<div class="col-6 book-det-right ps-0 pe-0">
											<p>+971 234 45678</p>
										</div>
									</div>
								</div>
								
								<div class="col-sm-12 book-details-main">
									<div class="row m-0">
										<div class="col-4 book-det-left ps-0 pe-0">
											<p>Address</p>
										</div>
										<div class="col-8 book-det-right ps-0 pe-0">
											<p>Al Fattan Tower<br />Jumeirah Beach Residence<br />Dubai - UAE</p>
										</div>
									</div>
								</div>

								
							</div>



						   
						   <div class="col-lg-4 col-md-6 booking-success-middle">
								<div class="col-sm-12 book-details-main pb-2">
									<h4>Service Details</h4>
								</div>
								
								<div class="col-sm-12 book-details-main">
									<div class="row m-0">
										<div class="col-6 book-det-left ps-0 pe-0">
											<p>Service</p>
										</div>
										<div class="col-6 book-det-right ps-0 pe-0">
											<p>Home Cleaning</p>
										</div>
									</div>
								</div>
								
								<div class="col-sm-12 book-details-main">
									<div class="row m-0">
										<div class="col-6 book-det-left ps-0 pe-0">
											<p>Service Details</p>
										</div>
										<div class="col-6 book-det-right ps-0 pe-0">
											<p>3x Cupboard Cleaning</p>
										</div>
									</div>
								</div>
								
								<div class="col-sm-12 book-details-main">
									<div class="row m-0">
										<div class="col-6 book-det-left ps-0 pe-0">
											<p>Frequency</p>
										</div>
										<div class="col-6 book-det-right ps-0 pe-0">
											<p>One Time Service</p>
										</div>
									</div>
								</div>
								
								<div class="col-sm-12 book-details-main">
									<div class="row m-0">
										<div class="col-6 book-det-left ps-0 pe-0">
											<p>Duration</p>
										</div>
										<div class="col-6 book-det-right ps-0 pe-0">
											<p>2 hours</p>
										</div>
									</div>
								</div>
								
								<div class="col-sm-12 book-details-main">
									<div class="row m-0">
										<div class="col-5 book-det-left ps-0 pe-0">
											<p>Date & Time</p>
										</div>
										<div class="col-7 book-det-right ps-0 pe-0">
											<p>16 Oct, 2023,<br />
												05:00 pm  to  07:00 pm</p>
										</div>
									</div>
								</div>
								
								<div class="col-sm-12 book-details-main">
									<div class="row m-0">
										<div class="col-7 book-det-left ps-0 pe-0">
											<p>Number of Professionals</p>
										</div>
										<div class="col-5 book-det-right ps-0 pe-0">
											<p>5</p>
										</div>
									</div>
								</div>
								
								<div class="col-sm-12 book-details-main">
									<div class="row m-0">
										<div class="col-6 book-det-left ps-0 pe-0">
											<p>Material</p>
										</div>
										<div class="col-6 book-det-right ps-0 pe-0">
											<p>No</p>
										</div>
									</div>
								</div>
								
								<div class="col-sm-12 book-details-main">
									<div class="row m-0">
										<div class="col-5 book-det-left ps-0 pe-0">
											<p>Crew</p>
										</div>
										<div class="col-7 book-det-right ps-0 pe-0">
											<p>Elizabeth Delya</p>
										</div>
									</div>
								</div>
							</div>
						   
						   
						   
						   
						   <div class="col-lg-4 col-md-6 booking-success-middle">
								<div class="col-sm-12 book-details-main pb-2">
									<h4>Price Details</h4>
								</div>
								
								<div class="col-sm-12 book-details-main">
									<div class="row m-0">
										<div class="col-6 book-det-left ps-0 pe-0">
											<p>Payment Method</p>
										</div>
										<div class="col-6 book-det-right ps-0 pe-0">
											<p>Credit Card</p>
										</div>
									</div>
								</div>
								
								<div class="col-sm-12 book-details-main">
									<div class="row m-0">
										<div class="col-6 book-det-left ps-0 pe-0">
											<p>Payment Status</p>
										</div>
										<div class="col-6 book-det-right ps-0 pe-0">
											<p>Paid</p>
										</div>
									</div>
								</div>
								
								<div class="col-sm-12 book-details-main">
									<div class="row booking-amount m-0">
										<div class="col-6 book-det-left ps-0 pe-0">
											<p>Service Fee</p>
										</div>
										<div class="col-6 book-det-right ps-0 pe-0">
											<p><span>AED</span> 526</p>
										</div>
									</div>
								</div>
								
								<div class="col-sm-12 book-details-main">
									<div class="row booking-amount m-0">
										<div class="col-6 book-det-left ps-0 pe-0">
											<p>Total (inc VAT 5%)</p>
										</div>
										<div class="col-6 book-det-right ps-0 pe-0">
											<p><span>AED</span> 4.67</p>
										</div>
									</div>
								</div>
								
								<div class="col-sm-12 book-details-main">
									 <div class="row total-price m-0">
									      <div class="col-7 book-det-left ps-0 pe-0"><p>Total</p></div>
									      <div class="col-5 book-det-right ps-0 pe-0"><p><span>AED</span> 98.00</p></div>
								     </div>
								</div>

								
							</div>
							
				     </div>	
					 
					 
					 <div class="d-flex booking-alert mt-5">
							  <div class="booking-alert-icon"><i class="fa  fa-volume-control-phone"></i></div>
							  <div class="booking-alert-cont flex-grow-1"><p><strong>Get Help</strong><br><span>If you have any problem with this boking, please connect with us</span></p></div>
							  <div class="booking-alert-btn"><a href="booking-help.php">Supporting Team</a></div>
							</div>
							
									
				 </div>
			</div>
		</div>  
    </section>

</div>

<?php require_once('include/footer.php') ?>
          
</body>
</html>
