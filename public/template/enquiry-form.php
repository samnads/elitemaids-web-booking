	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Emaid Booking</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="theme-color" content="#0c3995">
<link rel="icon" type="image/png" href="images/favicon.png"/>

<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" >
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" href="css/animation.css"/>
<link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">

</head>

<body>



<div class="wrapper-main">
	
    
    <?php require_once('include/header.php') ?>
	
    
    <section>
		<div class="container">
			<div class="row inner-wrapper enquiry-form-main v-center m-0">
                 
				 <div class="col-lg-6 col-md-9 col-sm-12 enquiry-box m-auto">
				 
					  <div class="col-sm-12 popup-head-text"><h4>Enquiry Form</h4></div>
					  
					  <div class="col-sm-12 enquiry-option">
						   <ul>
						       <li class="call-us-enquiry"><a href="tel:123456789" target="_blank"><i class="fa fa-phone"></i> Call Us</a></li>
							   <li class="whatsapp-enquiry"><a href="https://api.whatsapp.com/send?phone=123456789" target="_blank"><i class="fa fa-whatsapp"></i> Whatsapp</a></li>
							   <li class="email-enquiry"><a href="mailto:user@gmail.com" target="_blank"><i class="fa fa-envelope-o"></i> Email</a></li>
						   </ul>
					  </div>

					  <div class="col-sm-12 text-field-main">
						   <p>Note</p>
						   <textarea name="" cols="" rows="" class="text-field-big" placeholder=""></textarea>
					  </div>
					  
					  <div class="col-sm-6 frequency-main pt-3">
					       <input value="Submit" class="text-field-btn" type="submit">
					  </div>
					 
				 </div>
				 
			</div>
		</div>  
    </section>

</div>

<?php require_once('include/footer.php') ?>
          
</body>
</html>
