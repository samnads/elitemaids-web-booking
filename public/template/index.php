<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Emaid Booking</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="theme-color" content="#0c3995">
<link rel="icon" type="image/png" href="images/favicon.png"/>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" >
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" href="css/animation.css"/>
<link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
</head>

<body class="home-page">
<div class="wrapper-main">

	<?php require_once('include/header.php') ?>
	
	
	<section class="banner-section">
		<!--<div class="container">-->
			<div class="row banner-wrapper m-0">
				<div id="banner" class="owl-carousel owl-theme p-0">
					<div class="item position-relative">
						<a href="#">
							 <img src="images/booking-banner1.jpg" class="object-fit_cover pc-view" alt="" />
							 <img src="images/booking-banner-mob1.jpg" class="object-fit_cover mob-view" alt="" />
						</a>
					</div>
					
					<div class="item position-relative">
						<a href="#">
							 <img src="images/booking-banner2.jpg" class="object-fit_cover pc-view" alt="" />
							 <img src="images/booking-banner-mob2.jpg" class="object-fit_cover mob-view" alt="" />
						</a>
					</div>
					
					
				</div>
			</div>
		<!--</div>-->
	</section>
	
	
	
	<section class="all-service-wrapper">
		<div class="container">
			<h3>General Cleaning</h3>
			<div class="row m-0">
				<div id="service1" class="owl-carousel owl-theme p-0"> <a href="booking-service-details.php">
					<div class="item position-relative">
						<div class="col-sm-12 service-image"><img src="images/house-cleaning.jpg" class="" alt="" /></div>
						<div class="col-sm-12 service-text">
							<h3>House Cleaning</h3>
						</div>
					</div>
					</a> <a href="furniture-cleaning.php">
					<div class="item position-relative">
						<div class="col-sm-12 service-image"><img src="images/house-cleaning.jpg" class="" alt="" /></div>
						<div class="col-sm-12 service-text">
							<h3>Furniture Cleaning</h3>
						</div>
					</div>
					</a> <a href="booking-service-details.php">
					<div class="item position-relative">
						<div class="col-sm-12 service-image"><img src="images/house-cleaning.jpg" class="" alt="" /></div>
						<div class="col-sm-12 service-text">
							<h3>Deep Cleaning</h3>
						</div>
					</div>
					</a> <a href="booking-service-details.php">
					<div class="item position-relative">
						<div class="col-sm-12 service-image"><img src="images/house-cleaning.jpg" class="" alt="" /></div>
						<div class="col-sm-12 service-text">
							<h3>AC Cleaning</h3>
						</div>
					</div>
					</a> <a href="booking-service-details.php">
					<div class="item position-relative">
						<div class="col-sm-12 service-image"><img src="images/house-cleaning.jpg" class="" alt="" /></div>
						<div class="col-sm-12 service-text">
							<h3>Car Wash</h3>
						</div>
					</div>
					</a> </div>
			</div>
		</div>
	</section>
	
	
	
	<section class="all-service-wrapper">
		<div class="container">
			<h3>Salon & Spa at Home</h3>
			<div class="row m-0">
				<div id="service2" class="owl-carousel owl-theme p-0"> <a href="furniture-cleaning.php">
					<div class="item position-relative">
						<div class="col-sm-12 service-image"><img src="images/spa.jpg" class="" alt="" /></div>
						<div class="col-sm-12 service-text">
							<h3>Women's Salon</h3>
						</div>
					</div>
					</a> <a href="furniture-cleaning.php">
					<div class="item position-relative">
						<div class="col-sm-12 service-image"><img src="images/spa.jpg" class="" alt="" /></div>
						<div class="col-sm-12 service-text">
							<h3>Women's Spa</h3>
						</div>
					</div>
					</a> <a href="furniture-cleaning.php">
					<div class="item position-relative">
						<div class="col-sm-12 service-image"><img src="images/spa.jpg" class="" alt="" /></div>
						<div class="col-sm-12 service-text">
							<h3>Premium Men's Salon</h3>
						</div>
					</div>
					</a> <a href="furniture-cleaning.php">
					<div class="item position-relative">
						<div class="col-sm-12 service-image"><img src="images/spa.jpg" class="" alt="" /></div>
						<div class="col-sm-12 service-text">
							<h3>Men's Spa</h3>
						</div>
					</div>
					</a> <a href="furniture-cleaning.php">
					<div class="item position-relative">
						<div class="col-sm-12 service-image"><img src="images/spa.jpg" class="" alt="" /></div>
						<div class="col-sm-12 service-text">
							<h3>Men's Spa</h3>
						</div>
					</div>
					</a> </div>
			</div>
		</div>
	</section>
	
	
	
	<section class="all-service-wrapper">
		<div class="container">
			<h3>Healthcare at Home</h3>
			<div class="row m-0">
				<div id="service3" class="owl-carousel owl-theme p-0"> <a href="furniture-cleaning.php">
					<div class="item position-relative">
						<div class="col-sm-12 service-image"><img src="images/healthcare.jpg" class="" alt="" /></div>
						<div class="col-sm-12 service-text">
							<h3>Lab Tests at Home</h3>
						</div>
					</div>
					</a> <a href="booking-service-details.php">
					<div class="item position-relative">
						<div class="col-sm-12 service-image"><img src="images/healthcare.jpg" class="" alt="" /></div>
						<div class="col-sm-12 service-text">
							<h3>IV Therapy at Hom</h3>
						</div>
					</div>
					</a> <a href="booking-service-details.php">
					<div class="item position-relative">
						<div class="col-sm-12 service-image"><img src="images/healthcare.jpg" class="" alt="" /></div>
						<div class="col-sm-12 service-text">
							<h3>Doctor Consultations</h3>
						</div>
					</div>
					</a> <a href="booking-service-details.php">
					<div class="item position-relative">
						<div class="col-sm-12 service-image"><img src="images/healthcare.jpg" class="" alt="" /></div>
						<div class="col-sm-12 service-text">
							<h3>Lab Tests at Home</h3>
						</div>
					</div>
					</a> <a href="booking-service-details.php">
					<div class="item position-relative">
						<div class="col-sm-12 service-image"><img src="images/healthcare.jpg" class="" alt="" /></div>
						<div class="col-sm-12 service-text">
							<h3>IV Therapy at Hom</h3>
						</div>
					</div>
					</a> <a href="booking-service-details.php">
					<div class="item position-relative">
						<div class="col-sm-12 service-image"><img src="images/healthcare.jpg" class="" alt="" /></div>
						<div class="col-sm-12 service-text">
							<h3>Doctor Consultations</h3>
						</div>
					</div>
					</a> </div>
			</div>
		</div>
	</section>
	
	
	<section class="packages-wrapper">
		<div class="container">
			<h3>Cleaning Packages</h3>
			<div class="row">
	
				<div class="col-lg-3 col-md-4 col-sm-4 booking-package-thumb-main">
					<input id="add-ons1" value="cash" name="add-ons" class="" type="checkbox">
					<label for="add-ons1">
					<div class="add-ons-scroll-thumb">
						<div class="add-ons-scroll-image"><img src="images/add-ons1.jpg" alt=""></div>
						<div class="add-ons-scroll-cont-main">
							<div class="add-ons-scroll-cont">
								<h4>Cupboard Cleaning</h4>
								<p><strong>Messy cupboards?</strong> Lorem Ipsum is simply dummy teof the printing... <a href="#">Learn More</a></p>
							</div>
							<div class="add-ons-scroll-price">AED <span> 55 </span> 40</div>
							<div class="add-ons-scroll-btn">
								<div class="addon-btn-main"> <a class="sp-btn">Add</a> </div>
								<div class="addon-btn-count">
									<input value="" class="addon-btn-minus" type="submit">
									<input name="" value="1" class="addon-text-field" type="text">
									<input value="" class="addon-btn-plus" type="submit">
								</div>
							</div>
						</div>
					</div>
					</label>
				</div>
			    
				
				<div class="col-lg-3 col-md-4 col-sm-4 booking-package-thumb-main">
					<input id="add-ons2" value="cash" name="add-ons" class="" type="checkbox">
					<label for="add-ons2">
					<div class="add-ons-scroll-thumb">
						<div class="add-ons-scroll-image"><img src="images/add-ons1.jpg" alt=""></div>
						<div class="add-ons-scroll-cont-main">
							<div class="add-ons-scroll-cont">
								<h4>Cupboard Cleaning</h4>
								<p><strong>Messy cupboards?</strong> Lorem Ipsum is simply dummy teof the printing... <a href="#">Learn More</a></p>
							</div>
							<div class="add-ons-scroll-price">AED <span> 55 </span> 40</div>
							<div class="add-ons-scroll-btn">
								<div class="addon-btn-main"> <a class="sp-btn">Add</a> </div>
								<div class="addon-btn-count">
									<input value="" class="addon-btn-minus" type="submit">
									<input name="" value="1" class="addon-text-field" type="text">
									<input value="" class="addon-btn-plus" type="submit">
								</div>
							</div>
						</div>
					</div>
					</label>
				</div>
				
				
				<div class="col-lg-3 col-md-4 col-sm-4 booking-package-thumb-main">
					<input id="add-ons3" value="cash" name="add-ons" class="" type="checkbox">
					<label for="add-ons3">
					<div class="add-ons-scroll-thumb">
						<div class="add-ons-scroll-image"><img src="images/add-ons1.jpg" alt=""></div>
						<div class="add-ons-scroll-cont-main">
							<div class="add-ons-scroll-cont">
								<h4>Cupboard Cleaning</h4>
								<p><strong>Messy cupboards?</strong> Lorem Ipsum is simply dummy teof the printing... <a href="#">Learn More</a></p>
							</div>
							<div class="add-ons-scroll-price">AED <span> 55 </span> 40</div>
							<div class="add-ons-scroll-btn">
								<div class="addon-btn-main"> <a class="sp-btn">Add</a> </div>
								<div class="addon-btn-count">
									<input value="" class="addon-btn-minus" type="submit">
									<input name="" value="1" class="addon-text-field" type="text">
									<input value="" class="addon-btn-plus" type="submit">
								</div>
							</div>
						</div>
					</div>
					</label>
				</div>
				
				
				<div class="col-lg-3 col-md-4 col-sm-4 booking-package-thumb-main">
					<input id="add-ons4" value="cash" name="add-ons" class="" type="checkbox">
					<label for="add-ons4">
					<div class="add-ons-scroll-thumb">
						<div class="add-ons-scroll-image"><img src="images/add-ons1.jpg" alt=""></div>
						<div class="add-ons-scroll-cont-main">
							<div class="add-ons-scroll-cont">
								<h4>Cupboard Cleaning</h4>
								<p><strong>Messy cupboards?</strong> Lorem Ipsum is simply dummy teof the printing... <a href="#">Learn More</a></p>
							</div>
							<div class="add-ons-scroll-price">AED <span> 55 </span> 40</div>
							<div class="add-ons-scroll-btn">
								<div class="addon-btn-main"> <a class="sp-btn">Add</a> </div>
								<div class="addon-btn-count">
									<input value="" class="addon-btn-minus" type="submit">
									<input name="" value="1" class="addon-text-field" type="text">
									<input value="" class="addon-btn-plus" type="submit">
								</div>
							</div>
						</div>
					</div>
					</label>
				</div>
				
				
				<div class="col-lg-3 col-md-4 col-sm-4 booking-package-thumb-main">
					<input id="add-ons5" value="cash" name="add-ons" class="" type="checkbox">
					<label for="add-ons5">
					<div class="add-ons-scroll-thumb">
						<div class="add-ons-scroll-image"><img src="images/add-ons1.jpg" alt=""></div>
						<div class="add-ons-scroll-cont-main">
							<div class="add-ons-scroll-cont">
								<h4>Cupboard Cleaning</h4>
								<p><strong>Messy cupboards?</strong> Lorem Ipsum is simply dummy teof the printing... <a href="#">Learn More</a></p>
							</div>
							<div class="add-ons-scroll-price">AED <span> 55 </span> 40</div>
							<div class="add-ons-scroll-btn">
								<div class="addon-btn-main"> <a class="sp-btn">Add</a> </div>
								<div class="addon-btn-count">
									<input value="" class="addon-btn-minus" type="submit">
									<input name="" value="1" class="addon-text-field" type="text">
									<input value="" class="addon-btn-plus" type="submit">
								</div>
							</div>
						</div>
					</div>
					</label>
				</div>
				
				
				<div class="col-lg-3 col-md-4 col-sm-4 booking-package-thumb-main">
					<input id="add-ons6" value="cash" name="add-ons" class="" type="checkbox">
					<label for="add-ons6">
					<div class="add-ons-scroll-thumb">
						<div class="add-ons-scroll-image"><img src="images/add-ons1.jpg" alt=""></div>
						<div class="add-ons-scroll-cont-main">
							<div class="add-ons-scroll-cont">
								<h4>Cupboard Cleaning</h4>
								<p><strong>Messy cupboards?</strong> Lorem Ipsum is simply dummy teof the printing... <a href="#">Learn More</a></p>
							</div>
							<div class="add-ons-scroll-price">AED <span> 55 </span> 40</div>
							<div class="add-ons-scroll-btn">
								<div class="addon-btn-main"> <a class="sp-btn">Add</a> </div>
								<div class="addon-btn-count">
									<input value="" class="addon-btn-minus" type="submit">
									<input name="" value="1" class="addon-text-field" type="text">
									<input value="" class="addon-btn-plus" type="submit">
								</div>
							</div>
						</div>
					</div>
					</label>
				</div>
				
				
				<div class="col-lg-3 col-md-4 col-sm-4 booking-package-thumb-main">
					<input id="add-ons7" value="cash" name="add-ons" class="" type="checkbox">
					<label for="add-ons7">
					<div class="add-ons-scroll-thumb">
						<div class="add-ons-scroll-image"><img src="images/add-ons1.jpg" alt=""></div>
						<div class="add-ons-scroll-cont-main">
							<div class="add-ons-scroll-cont">
								<h4>Cupboard Cleaning</h4>
								<p><strong>Messy cupboards?</strong> Lorem Ipsum is simply dummy teof the printing... <a href="#">Learn More</a></p>
							</div>
							<div class="add-ons-scroll-price">AED <span> 55 </span> 40</div>
							<div class="add-ons-scroll-btn">
								<div class="addon-btn-main"> <a class="sp-btn">Add</a> </div>
								<div class="addon-btn-count">
									<input value="" class="addon-btn-minus" type="submit">
									<input name="" value="1" class="addon-text-field" type="text">
									<input value="" class="addon-btn-plus" type="submit">
								</div>
							</div>
						</div>
					</div>
					</label>
				</div>
				
				
				<div class="col-lg-3 col-md-4 col-sm-4 booking-package-thumb-main">
					<input id="add-ons8" value="cash" name="add-ons" class="" type="checkbox">
					<label for="add-ons8">
					<div class="add-ons-scroll-thumb">
						<div class="add-ons-scroll-image"><img src="images/add-ons1.jpg" alt=""></div>
						<div class="add-ons-scroll-cont-main">
							<div class="add-ons-scroll-cont">
								<h4>Cupboard Cleaning</h4>
								<p><strong>Messy cupboards?</strong> Lorem Ipsum is simply dummy teof the printing... <a href="#">Learn More</a></p>
							</div>
							<div class="add-ons-scroll-price">AED <span> 55 </span> 40</div>
							<div class="add-ons-scroll-btn">
								<div class="addon-btn-main"> <a class="sp-btn">Add</a> </div>
								<div class="addon-btn-count">
									<input value="" class="addon-btn-minus" type="submit">
									<input name="" value="1" class="addon-text-field" type="text">
									<input value="" class="addon-btn-plus" type="submit">
								</div>
							</div>
						</div>
					</div>
					</label>
				</div>

            </div>
        </div>
    </section>



	<section class="app-download-wrapper">
		<div class="container">
			<div class="row">
		        <div class="col-sm-6 app-download-left v-center">
				     <div> 
						  <h2>We are  Available on<br />App Store & Google Play</h2>
						  <p>Download the app from Google Play or App Store and start enjoying the benefits right away.</p>
						  <a href="#"><img src="images/android.jpg" alt="" /></a>
						  <a href="#"><img src="images/iOS.jpg" alt="" /></a>
					  </div>
				 </div>
				 <div class="col-sm-6 app-download-right"><img src="images/app.png" alt="" /></div>
            </div>
        </div>
    </section>
	
</div>

<?php require_once('include/footer.php') ?>

</body>
</html>
