	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Emaid Booking</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="theme-color" content="#0c3995">
<link rel="icon" type="image/png" href="images/favicon.png"/>

<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" >
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" href="css/animation.css"/>
<link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">

</head>

<body>

<div class="wrapper-main">
	
    
    <?php require_once('include/header.php') ?>
	
    
    <section>
		<div class="container">
			<div class="row inner-wrapper m-0">
			     <div class="col-sm-12 top-steps-section">
					  <div class="d-flex page-title-section">
						  <div class="booking-page-title flex-grow-1"><h3>Personal Details</h3></div>
						  <div class="step-back-icon"><a href="my-account.php" class="back-arrow" title="Click to Back">Back</a></div>
					  </div>
				 </div>
				 
				 
				 
                 <div class="col-sm-12 my-account-wrapper">
					  
					  <div class="row m-0 pt-1">
					       <div class="col-md-6 col-sm-12 n-personal-details m-auto">
                           <div class="col-sm-12 personal-details-section">
								<div class="d-flex personal-details-main">
									 <div class="pd-icon-main pd-name-icon v-center"><img src="images/user-icon7.png" alt="" /></div>
									 <div class="pd-content-main flex-grow-1">
									      <p>Full Name</p>
										  <input name="" placeholder="" class="text-field" value="Mathew Thomas" type="text" disabled="">
									 </div>
									 <div class="pd-edit-main d-flex align-items-end">
										  <label><i class="fa fa-pencil"></i></label>
									 </div>
								</div>
						   </div>
						   
						   <div class="col-sm-12 personal-details-section">
								<div class="d-flex personal-details-main edit-details">
									 <div class="pd-icon-main pd-phone-icon v-center"><img src="images/user-icon8.png" alt="" /></div>
									 <div class="pd-content-main flex-grow-1">
									      <p>Full Name</p>
										  <input name="" placeholder="" class="text-field" value="+971 2345 6789" type="text">
									 </div>
									 <div class="pd-edit-main d-flex align-items-end">
										  <label><i class="fa fa-pencil"></i></label>
									 </div>
								</div>
						   </div>
						   
						   <div class="col-sm-12 personal-details-section border-0">
								<div class="d-flex personal-details-main">
									 <div class="pd-icon-main  pd-email-icon v-center"><img src="images/user-icon9.png" alt="" /></div>
									 <div class="pd-content-main flex-grow-1">
									      <p>Full Name</p>
										  <input name="" placeholder="" class="text-field" value="mathewthomas@gmail.com" type="text" disabled="">
									 </div>
									 <div class="pd-edit-main d-flex align-items-end">
										  <label><i class="fa fa-pencil"></i></label>
									 </div>
								</div>
						   </div>
						   
						   
							<div class="col-sm-12 booking-main-btn-section n-personl-details-main p-0 pt-4">
								 <div class="row m-0">
									   <div class="col-lg-5 col-md-4 col-sm-6 p-0 pt-3">
											<input value="Update" class="text-field-btn" type="submit">
									   </div>
								 </div>
							</div>
						  </div> 
					  </div>
				 </div>
			</div>
		</div>  
    </section>

</div>

<?php require_once('include/footer.php') ?>
          
</body>
</html>
