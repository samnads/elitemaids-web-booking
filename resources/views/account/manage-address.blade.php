@extends('layouts.main')
@section('title', 'Manage Address')
@section('content')
    <section>
        <div class="container">
            <div class="row inner-wrapper m-0">
                <div class="col-sm-12 top-steps-section">
                    <div class="d-flex page-title-section">
                        <div class="booking-page-title flex-grow-1">
                            <h3>Manage Address</h3>
                        </div>
                        <div class="step-back-icon"><a href="{{ url('profile') }}" class="back-arrow"
                                title="Click to Back">Back</a></div>
                        <div class="booking-steps">&nbsp;&nbsp; <a class="sp-btn show-add-address-popup"
                                data-action="new-address-popup">Add New</a></div>
                    </div>
                </div>
                <div class="col-sm-12 my-account-wrapper">
                    <form id="default_address_form" novalidate="novalidate">
                        <div class="row m-0 pt-3">
                            <div class="col-sm-8 n-personal-details m-auto" id="profile-address-list-holder">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('styles')
@endpush
@push('scripts')
    <script type="text/javascript" src="{{ asset('js/profile.js?v=') . Config::get('version.js') }}"></script>
    <script>
        $(document).ready(function() {
            fetchAddressList();
        });
    </script>
@endpush
