<div class="col-sm-12 top-steps-section step-1">
    <div class="d-flex page-title-section">
        <div class="booking-page-title flex-grow-1">
            <h3>Service Details</h3>
        </div>
        <div class="step-back-icon"><a href="{{url('')}}" class="back-arrow" title="Click to Back">Step 1</a>
        </div>
        <div class="booking-steps"> of 4</div>
    </div>
</div>
<div class="col-lg-8 col-md-12 booking-form-left step-1">
    <div class="col-sm-12 how-many-hours-wrapper">
        <div class="col-sm-12 p-0 pb-2">
            <h4>How many hours do you need your professional to stay?</h4>
        </div>
        <div class="col-sm-12 booking-form-list p-0">
            <ul id="hours-count-holder">
                @for($i=$api_service_type_data['data']['min_working_hours'];$i<=$api_service_type_data['data']['max_working_hours'];$i++)
                <li><input id="hours-{{$i}}" value="{{$i}}" name="hours" class="" type="radio">
                    <label for="hours-{{$i}}">{{$i}}</label>
                </li>
                @endfor
            </ul>
        </div>
    </div>
    <div class="col-sm-12 how-mainy-housekeepers-wrapper">
        <div class="col-sm-12 p-0 pb-2">
            <h4>How many professionals do you need?</h4>
        </div>
        <div class="col-sm-12 booking-form-list p-0">
            <ul id="professionals-count-holder">
                @for($i=$api_service_type_data['data']['min_no_of_professionals'];$i<=$api_service_type_data['data']['max_no_of_professionals'];$i++)
                <li><input id="professionals-count-{{$i}}" value="{{$i}}" name="professionals_count" class="" type="radio">
                    <label for="professionals-count-{{$i}}">{{$i}}</label>
                </li>
                @endfor
            </ul>
        </div>
    </div>
    <div class="col-sm-12 cleaning-materials-wrapper">
        <div class="col-sm-12 p-0 pb-2">
            <h4>Need cleaning materials?</h4>
        </div>
        <div class="col-sm-12 booking-form-list p-0">
            <ul id="cleaning-materials-holder">
                <li><input id="cleaning-materials1" value="0" name="cleaning_materials" class=""
                        type="radio">
                    <label for="cleaning-materials1">No, I have them</label>
                </li>

                <li><input id="cleaning-materials2" value="1" name="cleaning_materials" class=""
                        type="radio">
                    <label for="cleaning-materials2">Yes, Please</label>
                </li>
            </ul>
        </div>
    </div>
    @if($api_service_type_data['data']['info_html'] != '')
    <div class="col-sm-12 cleaning-materials-wrapper">
        <div class="col-sm-12 p-0 pb-2" id="info_html">
            {!!$api_service_type_data['data']['info_html']!!}
        </div>
    </div>
    @endif
</div>
