<div class="col-sm-12 top-steps-section step-2" style="display: none">
    <div class="d-flex page-title-section">
        <div class="booking-page-title flex-grow-1">
            <h3 id="title-step-2">Popular Add-ons</h3>
        </div>
        <div class="step-back-icon"><a href="javascript:void(0);" data-action="prev-step" data-step="2" class="back-arrow" title="Click to Back">Step
                2</a></div>
        <div class="booking-steps"> of 4</div>
    </div>
</div>
<div class="col-lg-8 col-md-12 booking-form-left step-2" style="display: none">
    <div class="col-sm-12 add-ons-wrapper">
        <div class="col-sm-12 p-0 pb-2">
            <h4>People also added</h4>
        </div>
        <div class="col-sm-12 add-ons-scroll-section">
            <div id="service-addons-holder" class="owl-carousel owl-theme p-0">

            </div>
        </div>
    </div>
    <div class="col-sm-12 cleaning-materials-wrapper">
        <div class="col-sm-12 p-0 pb-2">
            <h4>Any instructions or special requirements?</h4>
        </div>
        <div class="col-sm-12 booking-form-field p-0">
            <textarea name="instructions" rows="" class="text-field-big" spellcheck="false"
                placeholder="Write here..." maxlength="300"></textarea>
                <p class="text-muted" id="ins-char-left">300 characters left</p>
        </div>
    </div>
</div>