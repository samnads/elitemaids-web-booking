<div class="col-lg-4 col-md-5 booking-summary-section" id="booking-summary">
    <div class="col-sm-12 book-details-main mob-booking-title">
        <h3>Booking Summary</h3>
    </div>


    <div class="col-lg-11 col-md-12 booking-summary-main clearfix scroll">
        <div class="row m-0">
            <div class="col-sm-12 book-details-main mob-summary-title pb-2">
                <h3>Booking Summary</h3>
            </div>

            <div class="col-sm-12 book-details-main">
                <div class="row m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Service</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p>{{ $api_service_type_data['data']['service_type_name'] }}</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main">
                <div class="row m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Frequency</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p class="frequency">-</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main">
                <div class="row m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Duration</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p class="duration">-</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main">
                <div class="row m-0">
                    <div class="col-5 book-det-left ps-0 pe-0">
                        <p>Date & Time</p>
                    </div>
                    <div class="col-7 book-det-right ps-0 pe-0">
                        <p class="date">-</p>
                        <p class="time"></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main">
                <div class="row m-0">
                    <div class="col-7 book-det-left ps-0 pe-0">
                        <p>Number of Professionals</p>
                    </div>
                    <div class="col-5 book-det-right ps-0 pe-0">
                        <p class="professionals_count">-</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main">
                <div class="row m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Material</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p class="is_materials_included">-</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main instructions" style="display: none">
                <div class="row m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Instructions</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p class="instructions">-</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row book-details-main-set m-0 pt-5">
            <div class="col-sm-12 book-details-main pb-2">
                <h3>Payment Summary</h3>
            </div>
            <div class="col-sm-12 book-details-main">
                <div class="row booking-amount m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Service Fee</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p><span>AED</span> <calc-amount class="service_amount">00.00</calc-amount></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main cleaning_materials_amount" style="display: none">
                <div class="row booking-amount m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Material Fee</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p><span>AED</span> <calc-amount class="cleaning_materials_amount">00.00</calc-amount></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main discount_total" style="display: none">
                <div class="row booking-amount m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Discount</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p><span>AED</span> <calc-amount class="discount_total">00.00</calc-amount></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main">
                <div class="row booking-amount m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Taxable Amount</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p><span>AED</span> <calc-amount class="taxable_amount">00.00</calc-amount></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main">
                <div class="row booking-amount m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Total (inc VAT 5%)</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p><span>AED</span> <calc-amount class="taxed_amount">00.00</calc-amount></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main payment_type_charge" style="display: none">
                <div class="row booking-amount m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Convenience Fee</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p><span>AED</span> <calc-amount class="payment_type_charge">00.00</calc-amount></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main">
                <div class="row total-price m-0">
                    <div class="col-5 book-det-left ps-0 pe-0">
                        <p>Total</p>
                    </div>
                    <div class="col-7 book-det-right ps-0 pe-0">
                        <p><span>AED</span> <calc-amount class="total_payable">00.00</calc-amount></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>