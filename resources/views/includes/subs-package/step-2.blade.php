<div class="col-sm-12 top-steps-section step-2" style="display: none">
    <div class="d-flex page-title-section">
        <div class="booking-page-title flex-grow-1">
            <h3>Checkout</h3>
        </div>
        <div class="step-back-icon"><a href="javascript:void(0);" data-action="prev-step" data-step="2" class="back-arrow"
                title="Click to Back">Step
                2</a></div>
        <div class="booking-steps"> of 2</div>
    </div>
</div>
<div class="col-lg-8 col-md-12 booking-form-left step-2" style="display: none">
    <div class="col-sm-12 payment-method-wrapper pb-2">
        <div class="col-sm-12 p-0 pb-2">
            <h4>Payment method</h4>
        </div>
        <div class="col-sm-12 booking-form-list payment-method p-0">
            <ul id="payment-method-holder">
                @foreach ($api_data['payment_types'] as $key => $payment_type)
                    @if ($payment_type['show_in_web'] == 1)
                        @if (!in_array($payment_type['id'], hidePaymentModes('subs-package-model')))
                            <li class="pay-mode-li-{{ $payment_type['id'] }}">
                                <input id="payment-method-{{ $payment_type['id'] }}" value="{{ $payment_type['id'] }}"
                                    name="payment_method" class="" type="radio"
                                    {{ $payment_type['default'] == 1 ? 'checked' : '' }}>
                                <label for="payment-method-{{ $payment_type['id'] }}">
                                    <!-- <p>Payment by</p>{{ $payment_type['name'] }} -->
                                    <img
                                        src="{{ asset('images/payment-' . $payment_type['id'] . '.jpg?v=' . Config::get('version.img')) }}" />
                                </label>
                            </li>
                        @else
                            {{-- @if (in_array(session('customer_id'), debugPaymentModeForCustomers()))
                                <li class="{{ $payment_type['code'] }}-opt">
                                    <input id="payment-method-{{ $payment_type['id'] }}"
                                        value="{{ $payment_type['id'] }}" name="payment_method" class=""
                                        type="radio" {{ $payment_type['default'] == 1 ? 'checked' : '' }}>
                                    <label for="payment-method-{{ $payment_type['id'] }}">
                                        <p>Payment by</p>{{ $payment_type['name'] }}
                                    </label>
                                </li>
                            @endif --}}
                        @endif
                    @endif
                @endforeach
            </ul>
        </div>
    </div>
    <div class="col-sm-12 card-method-main mb-5 payment-type-2" style="display: none" id="card-details">
        <div class="row card-main-box m-0">
            <div class="col-sm-12 card-method-field">
                <p>Card Number</p>
                <input name="card_number" class="text-field" type="number">
            </div>

            <div class="col-sm-8 card-method-field">
                <p>Exp. Date (MM/YY)</p>
                <!--<input name="" class="text-field" type="number">-->
                <input id="datepicker" class="text-field w90 calendar" name="exp_date" type="text" autocomplete="off"
                    data-provide="datepicker">
            </div>

            <div class="col-sm-4 card-method-field">
                <p>CVV Number</p>
                <input name="cvv" class="text-field" type="number">
            </div>
        </div>
    </div>
    @if (@$tc_html != '')
        <div class="col-sm-12 cleaning-materials-wrapper">
            <div class="col-sm-12 p-0 pb-2">
                <h4>Terms & Conditions</h4>
            </div>
            <div class="col-sm-12 p-0 pb-2" id="terms_and_cond">
                {!! $tc_html !!}
            </div>
        </div>
    @endif
</div>
