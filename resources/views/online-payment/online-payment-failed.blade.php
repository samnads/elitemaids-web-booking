@extends('layouts.main')
@section('title', 'Invoice Payment Failed')
@section('content')
    <section>
        <div class="container">
            <div class="row inner-wrapper m-0">
                <div class="col-sm-12 booking-success-top booking-error">
                    <div class="col-sm-12 booking-success-image"><img src="{{ asset('images/booking-failed.gif') }}"
                            alt="" /></div>
                    <div class="col-sm-12 booking-success-title">
                        <h3>Payment failed</h3>
                    </div>
                    <div class="col-sm-12 booking-success-details">
                        <ul>
                            <li>
                                <div class="col-sm-12 booking-alert-main">
                                    <div class="d-flex booking-alert">
                                        <div class="booking-alert-icon"><i class="fa fa-calendar"></i></div>
                                        <div class="booking-alert-cont flex-grow-1">
                                            <p><span>Payment Status</span><br /><strong>Not Paid</strong></p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="col-sm-12 booking-alert-main">
                                    <div class="d-flex booking-alert">
                                        <div class="booking-alert-icon"><i class="fa fa-user-o"></i></div>
                                        <div class="booking-alert-cont flex-grow-1">
                                            <p><span>Invoice Ref.</span><br /><strong>{{ @$online_payment['inv_id'] ?: '-' }}</strong></p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="row m-0">
                        <div class="col-lg-4 col-md-6 booking-success-left">

                        </div>
                        <div class="col-lg-4 col-md-6 booking-success-middle">
                            <div class="col-sm-12 book-details-main pb-2">
                                <h4>Personal Details</h4>
                            </div>

                            <div class="col-sm-12 book-details-main">
                                <div class="row m-0">
                                    <div class="col-6 book-det-left ps-0 pe-0">
                                        <p>Name</p>
                                    </div>
                                    <div class="col-6 book-det-right ps-0 pe-0">
                                        <p>{{ @$customer['customer_name'] }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 book-details-main">
                                <div class="row m-0">
                                    <div class="col-6 book-det-left ps-0 pe-0">
                                        <p>Email ID</p>
                                    </div>
                                    <div class="col-6 book-det-right ps-0 pe-0">
                                        <p>{{ @$customer['email_address'] }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 book-details-main">
                                <div class="row m-0">
                                    <div class="col-6 book-det-left ps-0 pe-0">
                                        <p>Contact Number</p>
                                    </div>
                                    <div class="col-6 book-det-right ps-0 pe-0">
                                        <p>{{ @$customer['mobile_number_1'] }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 book-details-main">
                                <div class="row m-0">
                                    <div class="col-4 book-det-left ps-0 pe-0">
                                        <p>Address</p>
                                    </div>
                                    <div class="col-8 book-det-right ps-0 pe-0">
                                        <p>{{ @$customer_address['customer_address'] }}</p>
                                        <p>{{ @$customer_address['flat_no'] }}, {{ @$customer_address['building'] }}</p>
                                        <p>Dubai - UAE</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 book-details-main pb-2">
                                <h4>Price Details</h4>
                            </div>
                            <div class="col-sm-12 book-details-main">
                                <div class="row m-0">
                                    <div class="col-6 book-det-left ps-0 pe-0">
                                        <p>Price</p>
                                    </div>
                                    <div class="col-6 book-det-right ps-0 pe-0">
                                        <p>{{ number_format(@$online_payment['amount'],2,".",",") }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 book-details-main">
                                <div class="row m-0">
                                    <div class="col-6 book-det-left ps-0 pe-0">
                                        <p>Total</p>
                                    </div>
                                    <div class="col-6 book-det-right ps-0 pe-0">
                                        <p>{{ number_format(@$online_payment['amount'],2,".",",") }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 booking-success-middle mt-5 m-auto">
                        </div>
                        <div class="col-sm-12">

                            <div class="col-lg-4 col-md-6 pt-4 m-auto book-details-main">
                                <div class="col-sm-12 p-1">
                                    <a href="{{ url('payment?id='.@$customer['customer_id'].'&amount='.@$online_payment['amount'].'&message='.@$online_payment['description'].'') }}">
                                        <input value="Retry Payment" class="text-field-btn" type="submit">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('styles')
@endpush
@push('scripts')
@endpush
