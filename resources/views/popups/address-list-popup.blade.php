<div class="popup-main address-list-popup" id="address-list-popup">
    <form id="address-list-popup-form">
        <div class="row min-vh-100 m-0">
            <div class=" mx-auto my-auto shadow popup-main-cont">
                <div class="popup-close" data-action="close"><img src="{{ asset('images/el-close-white.png') }}"
                        alt=""></div>
                <div class="col-sm-12 popup-head-text">
                    <h4>Booking Address</h4>
                </div>
                <div class="row m-0">
                    <div class="col-sm-12 m-auto" id="addresses-list">
                    </div>
                </div>
                <div class="row m-0">
                    <div class="col-sm-12 m-auto" id="addresses-list">
                        <div class="col-sm-12 booking-main-btn-section p-0 pt-4">
                            <div class="row m-0">
                                <div class="col-lg-5 col-md-4 col-sm-6 p-0 pt-3">
                                    <button class="text-field-btn" type="submit">Select Address</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div><!-- Address Popup-->
