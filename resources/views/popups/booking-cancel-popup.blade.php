<div class="popup-main" id="booking-cancel-confirm">
    <form id="booking-cancel-form">
        <input name="booking_id" type="hidden" style="display: none" />
        <input name="date" type="hidden" style="display: none" />
        <div class="row min-vh-100 m-0">
            <div class=" mx-auto my-auto shadow popup-main-cont">
                <div class="popup-close" data-action="close"><img src="{{ asset('images/el-close-white.png') }}"
                        alt="">
                </div>
                <div class="col-sm-12 popup-head-text">
                    <h4>Confirm Cancel ?</h4>
                </div>
                <div class="row m-0">

                    <div class="col-sm-12 login-content p-0">
                        <p>Please confirm the cancellation of <span class="fw-bold text-secondary"
                                id="cancel-booking-frequency"></span> booking with Ref. No. <span
                                class="fw-bold text-red" id="cancel-booking-ref"></span> on date <span
                                class="fw-bold text-primary" id="cancel-booking-date"></span>, clicking on Yes will
                            cancel the schedule.</p>
                    </div>
                    <div class="col-sm-12 text-field-main">
                        <select class="form-control" name="cancel_reason">
                            <option value="">-- Select cancel reason --</option>
                            @foreach ($api_data['cancel_reasons'] as $key => $reason)
                                <option value="{{ $reason['reason'] }}">{{ $reason['reason'] }}</option>
                            @endforeach
                            <option value="Other">Other</option>
                        </select>
                    </div>
                    <div class="d-flex booking-alert">
                        <div class="booking-alert-icon"><i class="fa fa-exclamation-triangle text-danger"></i></div>
                        <div class="booking-alert-cont flex-grow-1">
                            <p><strong>Cancellation Policy</strong></p>
                            <p><span>Free cancellation within 12 hours, 50% charge applies between 12 hours and the time
                                    of booking.</span></p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 p-3">
                        <button class="text-field-btn bg-danger" type="submit">Yes</button>
                    </div>
                    <div class="col-sm-12 col-md-6 pt-3">
                        <button class="text-field-btn bg-success" type="button" data-action="close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
