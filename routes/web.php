<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\OnlinePaymentController;
use App\Http\Controllers\WebViewController;
use App\Http\Controllers\RedirectController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['validateSessionTokenIfExist']], function () {
    Route::get('/', [HomeController::class, 'home'])->name('home');
    /************************ dynamic routes from api *************************** */
    // follow these steps to change service type route urls
    // 1. change the web_url_slug of desired service in databse
    // 2. clear laravel route cache
    $api_data = customerApiCall('data', [], 'GET')['result'];
    foreach ($api_data['service_categories'] as $service_category) {
        foreach ($service_category['sub_categories'] as $service) {
            if ($service['service_type_model_id'] == 1) {
                Route::get('/' . $service['web_url_slug'] . '/{coupon_code?}', function () use ($service) {
                    return App::call('App\Http\Controllers\ServiceTypeModelController@normal_service', ['data' => $service]);
                })->name('service-type-' . $service['service_type_id']);
            } else if ($service['service_type_model_id'] == 2) {
                Route::get('/' . $service['web_url_slug'] . '/{coupon_code?}', function () use ($service) {
                    return App::call('App\Http\Controllers\ServiceTypeModelController@package_service', ['data' => $service]);
                })->name('service-type-' . $service['service_type_id']);
            } else if ($service['service_type_model_id'] == 3) {
                Route::get('/' . $service['web_url_slug'], function () use ($service) {
                    return App::call('App\Http\Controllers\ServiceTypeModelController@enquiry_service', ['data' => $service]);
                })->name('service-type-' . $service['service_type_id']);
            }
        }
        foreach ($api_data['subscription_packages_and_special_offers'] as $key => $package) {
            if (@$package['package_id']) {
                // dynamic package routes
                Route::get('/package/' . $package['package_id'], function () use ($package) {
                    return App::call('App\Http\Controllers\ServiceTypeModelController@subscription_package', ['data' => $package]);
                })->name('subscription-package-' . $package['package_id']);
            }
        }
    }
    /************************ dynamic routes from api end *********************** */
});
Route::group(['middleware' => ['validateSessionToken']], function () {
    Route::get('/profile', [ProfileController::class, 'profile'])->name('profile');
    Route::get('/profile/personal-details', [ProfileController::class, 'personal_details']);
    Route::post('/profile/personal-details', [ProfileController::class, 'personal_details']);
    Route::get('/profile/manage-address', [ProfileController::class, 'manage_address']);
    Route::get('/bookings/{filter}', [ProfileController::class, 'bookings']);
    //Route::get('/booking/success/{reference_id}', [ProfileController::class, 'booking_success'])->name('booking-success');
    //Route::get('/booking/failed/{reference_id}', [ProfileController::class, 'booking_failed'])->name('booking-failed');
    // Route::post('/profile-popup', [ProfileController::class,'profile_edit_popup']);

});
//
Route::get('/booking/success/{reference_id}', [ProfileController::class, 'booking_success'])->name('booking-success');
Route::get('/booking/failed/{reference_id}', [ProfileController::class, 'booking_failed'])->name('booking-failed');
//
Route::get('hijack-customer-login', [ApiController::class, 'hijack_customer_login']);
//
Route::get('webview/google-pay', [WebViewController::class, 'google_pay_webview']);
// invoice module routes
Route::get('invoice-payment', [InvoiceController::class, 'invoice_payment_entry']);
Route::post('save-invoice-pay', [InvoiceController::class, 'invoice_payment_save']);
Route::get('/invoice-payment/success/{payment_id}', [InvoiceController::class, 'invoice_success'])->name('invoice-success');
Route::get('/invoice-payment/failed/{payment_id}', [InvoiceController::class, 'invoice_failed'])->name('invoice-failed');
// payment link module routes
//Route::get('online-payment', [OnlinePaymentController::class, 'online_payment_entry']);
Route::get('payment', [OnlinePaymentController::class, 'online_payment_link_entry']);
Route::post('save-online-pay', [OnlinePaymentController::class, 'online_payment_link_save']);
Route::get('/online-payment/success/{payment_id}', [OnlinePaymentController::class, 'online_payment_success'])->name('online-payment-success');
Route::get('/online-payment/failed/{payment_id}', [OnlinePaymentController::class, 'online_payment_failed'])->name('online-payment-failed');
//
Route::any('/api/customer/{endpoint}', [ApiController::class, 'customer_api_call']);
Route::any('/api/telr/{endpoint}', [ApiController::class, 'telr_api_call'])->where('endpoint', '.*');
Route::any('/redirect/{action}', [RedirectController::class, 'redirect_based_on_action']);
Route::get('/clear-cache', function () {
    $exitCode = Artisan::call('cache:clear');
    if($exitCode == 0){
        return "Cache cleared successfully !";
    }
});
